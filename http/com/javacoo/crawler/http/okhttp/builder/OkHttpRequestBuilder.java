package com.javacoo.crawler.http.okhttp.builder;

import java.util.LinkedHashMap;
import java.util.Map;

import com.javacoo.crawler.http.okhttp.request.RequestCall;
/**
 * 抽象请求参数构建器
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @param <T>
 * @since 2018年2月7日下午1:40:11
 */
public abstract class OkHttpRequestBuilder<T extends OkHttpRequestBuilder> {
	/**请求URL*/
	protected String url;
	/**响应对象*/
	protected Object tag;
	/**请求头信息*/
	protected Map<String, String> headers;
	/**请求参数信息*/
	protected Map<String, String> params;
	/**请求ID*/
	protected int id;

	public T id(int id) {
		this.id = id;
		return (T) this;
	}

	public T url(String url) {
		this.url = url;
		return (T) this;
	}

	public T tag(Object tag) {
		this.tag = tag;
		return (T) this;
	}

	public T headers(Map<String, String> headers) {
		this.headers = headers;
		return (T) this;
	}

	public T addHeader(String key, String val) {
		if (this.headers == null) {
			headers = new LinkedHashMap<>();
		}
		headers.put(key, val);
		return (T) this;
	}

	public abstract RequestCall build();
}
