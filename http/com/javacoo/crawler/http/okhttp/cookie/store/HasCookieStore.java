package com.javacoo.crawler.http.okhttp.cookie.store;

public interface HasCookieStore {
	 CookieStore getCookieStore();
}
