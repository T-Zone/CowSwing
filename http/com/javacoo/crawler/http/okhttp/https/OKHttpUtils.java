package com.javacoo.crawler.http.okhttp.https;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import com.alibaba.fastjson.JSON;

import lombok.extern.slf4j.Slf4j;
import okhttp3.FormBody.Builder;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import sun.rmi.runtime.Log;

/**
 * http工具
 * @author hanjie.l
 *
 */
@Slf4j
public class OKHttpUtils {
	// 设置可访问所有的https网站
	private static Https.SSLParams sslParams = Https.getSslSocketFactory(null, null, null);
	private static final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(2, TimeUnit.SECONDS)
            .readTimeout(4, TimeUnit.SECONDS)
            .writeTimeout(4, TimeUnit.SECONDS)
            .hostnameVerifier(new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			}).sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager)
            .build();


	/**
	 * 发送表单格式的post请求
	 * @param url
	 * @param params
	 * @return
	 */
	public static String doFromPost(String url, Map<String, Object> params){
		Builder builder = new Builder();
		if(params != null){
			for(String key : params.keySet()){
				builder.add(key, params.get(key).toString());
			}
		}
		
		RequestBody requestBodyPost = builder.build();
		
		Request requestPost = new Request.Builder()
        .url(url)
        .post(requestBodyPost)
		.header("Content-Type", "application/x-www-form-urlencoded")
        .build();
		
		 Response response;
		try {
			response = okHttpClient.newCall(requestPost).execute();
			return response.body().string();
		} catch (IOException e) {
			e.printStackTrace();
			log.error("http请求异常", e.getMessage());
		}
		
		return null;
	}

	/**
	 * 发送json格式的post请求
	 * @param url
	 * @param params
	 * @return
	 */
	public static String doJsonPost(String url, Object params){
		
		RequestBody requestBodyPost = RequestBody.create(MediaType.parse("application/json"), JSON.toJSONString(params));
		
		Request requestPost = new Request.Builder()
        .url(url)
        .post(requestBodyPost)
		.header("Content-Type", "application/json")
        .build();
		
		 Response response;
		try {
			response = okHttpClient.newCall(requestPost).execute();
			return response.body().string();
		} catch (IOException e) {
			e.printStackTrace();
			log.error("http请求异常", e.getMessage());
		}
		
		return null;
	}
	
	/**
	 * 发送get请求
	 * @param url
	 * @param paramsMap
	 * @return
	 */
	public static String doGet(String url, Map<String, Object> paramsMap, String cookies, String referer) throws Exception{
		StringBuilder params = new StringBuilder();
		if(paramsMap != null){
			for(String key : paramsMap.keySet()){
				if(params.length() > 0){
					params.append("&");
				}
				params.append(key+"="+ paramsMap.get(key).toString());
			}
		}
		
		if(params.length() > 0){
			url = url + "?" + params.toString();
		}

		Request.Builder builder = new Request.Builder()
				.url(url)
				.get();

		if(cookies != null){
			builder.addHeader("Cookie", cookies);
		}
		if(referer != null){
			builder.addHeader("Referer", referer);
		}
		Request requestPost = builder.build();
		
		Response response;
		response = okHttpClient.newCall(requestPost).execute();
		return response.body().string();
	}
	
	public static void main(String agrs[]){
//		Map<String, Object> paramsMap = new HashMap<String, Object>();
//		paramsMap.put("pageNumber", 1);
//		paramsMap.put("pageSize", 20);
//		paramsMap.put("type", 2);
//		String doJsonPost = OKHttpUtils.doFromPost("http://api.114zhibo.com/yys-api/index/getNewsInformation", paramsMap);
//		System.out.println(doJsonPost);
	}
}
