package com.javacoo.crawler.http.okhttp.request;

import java.util.Map;

import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * get请求
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * 
 * @author DuanYong
 * @since 2018年2月7日下午1:52:12
 */
public class GetRequest extends OkHttpRequest {
	public GetRequest(String url, Object tag, Map<String, String> params, Map<String, String> headers, int id) {
		super(url, tag, params, headers, id);
	}

	@Override
	protected RequestBody buildRequestBody() {
		return null;
	}

	@Override
	protected Request buildRequest(RequestBody requestBody) {
		return builder.get().build();
	}
}
