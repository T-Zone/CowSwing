/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.core.launcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.core.context.CowSwingContextData;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * ILauncher接口Spring实现类,负责加载Spring配置文件
 * 
 * @author DuanYong
 * @since 2012-11-30上午9:27:56
 * @version 1.0
 */
public class SpringLauncherImpl implements ILauncher {
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javacoo.icrawler.launcher.ILauncher#launch()
	 */
	@Override
	public void launch() {
		Map<String, Map<String, String>> plugins = CowSwingContextData.getInstance().getPlugins();
		List<String> activePlugins = new ArrayList<>(10);
		activePlugins.add("resources/spring/applicationContext.xml");
		String[] tempPaths;
		for(Entry<String, Map<String, String>> entry : plugins.entrySet()){
			if(Boolean.valueOf(entry.getValue().get(Constant.PLUGIN_PROPERTIES_KY_ACTIVE))){
				if(StringUtils.isNotBlank(entry.getValue().get(Constant.PLUGIN_PROPERTIES_KY_BEANXMLPATH))){
					tempPaths = entry.getValue().get(Constant.PLUGIN_PROPERTIES_KY_BEANXMLPATH).split(",");
					for(String path : tempPaths){
						activePlugins.add(path);
					}
				}
			}
		}
		int i = 0;
		String[] contextPaths = new String[activePlugins.size()];
		for(String xmlPath : activePlugins){
			contextPaths[i] = xmlPath;
			i++;
		}
		CowSwingContextData.getInstance().setApplicationContext(new ClassPathXmlApplicationContext(contextPaths));
	}
	
    
}
