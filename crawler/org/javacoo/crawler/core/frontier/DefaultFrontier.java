package org.javacoo.crawler.core.frontier;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.javacoo.crawler.core.CrawlerController;
import org.javacoo.crawler.core.constants.Constants;
import org.javacoo.crawler.core.data.Task;
import org.javacoo.crawler.core.data.Url;
import org.javacoo.crawler.core.data.queue.SimpleTaskQueue;
import org.javacoo.crawler.core.data.queue.TaskQueue;
import org.javacoo.crawler.core.data.queue.UrlQueue;
import org.javacoo.crawler.core.data.uri.CrawlLinkURI;

/**
 * 边界控制器  接口 实现类
 * 主要是加载爬行种子链接并根据加载的种子链接初始化任务队列，以备线程控制器（ProcessorThreadPool）开启的任务执行线程（ProcessorThread）使用
 * @author javacoo
 * @since 2011-11-09
 */
public class DefaultFrontier implements Frontier{
	private static Log log =  LogFactory.getLog(DefaultFrontier.class);
	/**爬虫控制器*/
	private CrawlerController controller;
	/**计划UrlQueue对象*/
	private TaskQueue<CrawlLinkURI> waitUrlQueue = new UrlQueue();
	/**任务对象*/
	private TaskQueue<Task> taskQueue = new SimpleTaskQueue();
	/**任务总数*/
    private transient AtomicInteger taskSize = new AtomicInteger(0);
    @Override
	public synchronized void finished(Task task) {
		task.finished();
		waitUrlQueueToTaskQueue();
		log.info("已经完成任务："+this.controller.getCrawlScope().getResultNum()+"个");
		if(StringUtils.isNotBlank(this.controller.getCrawlScope().getGatherNum())){
			if(Integer.valueOf(this.controller.getCrawlScope().getGatherNum()) - 1 == this.controller.getCrawlScope().getResultNum()){
				taskQueue.clear();
			}
		}
	}
	/**
	 * 初始化
	 * <li>加载爬行种子链接</li>
	 * <li>初始化任务队列</li>
	 * @param c 控制器对象
	 */
    @Override
	public void initialize(CrawlerController c) {
        this.controller = c;
        loadSeeds();
        initTask();
	}
    /**
     * 初始化任务队列
     */
	private void initTask() {
		log.info("=========初始化任务队列=========");
		waitUrlQueueToTaskQueue();
		log.info("=========初始化任务队列结束,任务总数：========="
				+ taskQueue.getUnExecTaskNum() + "个");
	}
	/**
	 * 等待队列URL进入任务队列
	 */
	private void waitUrlQueueToTaskQueue() {
		CrawlLinkURI crawlLinkURI = null;
		while (!waitUrlQueueEmpty()) {
			crawlLinkURI = getWaitUrlQueue();
			addCrawlLinkURIToTask(crawlLinkURI);
		}
	}

    /**
     * 加载种子链接
     */
	private void loadSeeds() {
		log.info("=========开始加载种子链接=========");
		List<String> plans = this.controller.getCrawlScope().getSeeds();
		CrawlLinkURI url = null;
		if(this.controller.getCrawlScope().isGatherOrder()){
			for (int i = 0; i < plans.size(); i++) {
				populateCrawlLinkURI(url,plans.get(i));
			}
		}else{
			log.info("=========plans.size()：========="+plans.size());
			for (int i = plans.size() - 1; i >= 0; i--) {
				populateCrawlLinkURI(url,plans.get(i));
			}
		}
		log.info("=========加载种子链接结束,共加载种子数：========="+waitUrlQueue.getUnExecTaskNum()+"个");
	}
	/**
	 * 组装任务连接
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-3-1 下午8:38:13
	 * @version 1.0
	 * @exception 
	 * @param url
	 * @param urlStr
	 */
	private void populateCrawlLinkURI(CrawlLinkURI url,String urlStr){
		url = this.controller.getUriHelper().populateCrawlURI(null,urlStr,"");
		url.setUrl(new Url(urlStr, "", Constants.URL_TYPE_LINK,
				Constants.PATH_TYPE_0));
		url.setSeed(true);
		this.controller.getCrawlScope().setDefaultHost(url.getHost());
		this.controller.getCrawlScope().setDefaultPort(url.getPort());
		this.controller.getCrawlScope().setDefaultPath(url.getPath());
		addCrawlLinkURIToWait(url);
	}
    /**
     * 判断当前边界控制器任务队列是否为空
     */
    @Override
	public boolean isEmpty() {
		return taskIsEmpty();
	}
    /**
     * 取得下一个任务
     */
    @Override
	public Task next() {
		return getTask();
	}
	/**
	 * 组装任务队列
	 * 
	 * @param crawlLinkURI
	 */
    @Override
	public void addCrawlLinkURIToTask(CrawlLinkURI crawlLinkURI) {
		this.controller.getCrawlScope().incrementTaskNum();
		Task task = new Task();
		task.setCrawlURI(crawlLinkURI);
		task.setController(this.controller);
		task.getContentBean().setTitle(crawlLinkURI.getTitle());
		task.getContentBean().setUrl(crawlLinkURI.getUrl().getUrl());
		task.getContentBean().setAcquId(this.controller.getCrawlScope().getId());
		if(null != crawlLinkURI.getResURI().getUrl() && StringUtils.isNotEmpty(crawlLinkURI.getResURI().getUrl().getUrl())){
			task.getContentBean().setTitleImg(crawlLinkURI.getResURI().getUrl().getUrl());
		}
		task.getContentBean().getResCrawlURIList().add(crawlLinkURI.getResURI());
		addTask(task);
	}
	/**
	 * 添加URL对象进入等待队列
	 * 
	 * @param crawlLinkURI
	 */
    @Override
	public void addCrawlLinkURIToWait(CrawlLinkURI crawlLinkURI) {
		addWaitUrlQueue(crawlLinkURI);
		//addCrawlLinkURIToExec(crawlLinkURI);
	}
	/**
	 * 连接URL对象入队列
	 * @param url url对象
	 */
	private void addWaitUrlQueue(CrawlLinkURI url){
		waitUrlQueue.addUnExecTask(url);
	}
	/**
	 * URL对象入已完成集合
	 * 
	 * @param crawlLinkURI
	 *            URL对象
	 */
	private void addCrawlLinkURIToExec(CrawlLinkURI crawlLinkURI) {
		waitUrlQueue.addExecTask(crawlLinkURI);
	}
	/**
	 * 连接URL对象出队列
	 * @return url对象
	 */
	private CrawlLinkURI getWaitUrlQueue(){
		return waitUrlQueue.unExecTaskDeQueue();
	}
	/**
	 *  判断当前对象是否为空
	 * @return true/flase
	 */
	private boolean waitUrlQueueEmpty(){
		return waitUrlQueue.isEmpty();
	}
	/**
	 * 任务对象入队列
	 * @param task 任务对象
	 */
	private void addTask(Task task){
		if (taskQueue.addUnExecTask(task)) {
			
			if(!task.getCrawlURI().isSeed() && StringUtils.isNotBlank(task.getCrawlURI().getTitle())){
				incrementTaskSize();
			}else if(task.getController().getCrawlScope().isOnlyGatherList()){
				incrementTaskSize();
			}
			// 总数加1
			this.controller.getCrawlScope().incrementTotalNum();
			synchronized (controller.lock) {
				controller.lock.notifyAll();
			}
		}
	}
	/**
	 * 任务对象出队列
	 * @return 任务对象
	 */
	private Task getTask(){
		return taskQueue.unExecTaskDeQueue();
	}
	/**
	 *  判断当前对象是否为空
	 * @return true/flase
	 */
	private boolean taskIsEmpty(){
		return taskQueue.isEmpty();
	}
	/**
	 * 取得任务总数
	 * @return 任务总数
	 */
	@Override
	public int getTaskSize() {
		return taskSize.get();
	}
	private void incrementTaskSize(){
		taskSize.incrementAndGet();
	}
	/**
	 * 取得已执行任务总数
	 * @return 已执行任务总数
	 */
    @Override
	public int getExecTaskNum() {
		return taskQueue.getExecTaskNum();
	}
	/**
	 * 取得未执行任务总数
	 * @return 未执行任务总数
	 */
    @Override
	public int getUnExecTaskNum() {
		return taskQueue.getUnExecTaskNum();
	}
	/**
	 * 销毁对象
	 */
    @Override
	public void destory(){
		taskQueue.clear();
		waitUrlQueue.clear();
		controller = null;
	}
	

}
