package org.javacoo.crawler.core.thread.swaparea;

import org.javacoo.crawler.core.data.Task;

/**
 * 数据交换工具类
 * <p>说明:</p>
 * <li></li>
 * @auther DuanYong
 * @since 2016年5月15日下午7:22:26
 */
public class SwapAreaUtils{
	private static SwapAreaManager swapAreaManager = new DefaultSwapAreaManager();


	public static SwapArea buildNewSwapArea() {
		return swapAreaManager != null ? swapAreaManager.buildNewSwapArea() : null;
	}

	public static SwapArea getCurrentSwapArea() {
		return swapAreaManager != null ? swapAreaManager.getCurrentSwapArea() : null;
	}

	public static SwapArea releaseCurrentSwapArea() {
		return swapAreaManager != null ? swapAreaManager.releaseCurrentSwapArea() : null;
	}
	
	public static void setTask(Task task) {
		SwapArea currentSwapArea = getCurrentSwapArea();
		if (currentSwapArea != null){
			currentSwapArea.setTask(task);
		}
	}
	public static Task getTask() {
		SwapArea currentSwapArea = getCurrentSwapArea();
		return currentSwapArea == null ? null : currentSwapArea.getTask();
	}
}
