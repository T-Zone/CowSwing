package org.javacoo.crawler.core.data.uri;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.javacoo.crawler.core.data.Url;


/**
 * 爬虫URI对象
 * @author javacoo
 * @since 2012-04-21
 */
public class CrawlLinkURI implements CrawlURI{
	/**来源URI*/
	private CrawlURI parentURI;
	/**URL全路径*/
	private Url url;
	/**主机*/
	private String host;
	/**端口*/
	private int port;
	/**路径：除去主机和端口剩余部分*/
	private String rawPath;
	/**是否是种子*/
	private boolean isSeed = false;
	/**标题*/
	private String title;
	/**类型：连接和资源*/
	private String type;
	/**源资对象*/
	private CrawlResURI resURI = new CrawlResURI();
	/**路径类型，绝对:0,相对根路径:1,相对当前路径:2*/
	private String pathType;
	/**资源描述*/
	private String desc;
	/**当前URL 深度*/
	private int depth = 0;
	/**访问是否成功*/
	private boolean success = true;
	/**重试次数*/
	private transient int tryNum = 0;
	/**状态码*/
    private transient String statusCode;
    /**状态描述*/
    private transient String statusDesc;
    /**最后更新时间*/
    private transient Date lastModified;
    /**编码*/
    private transient String encoding;
    /**原始HTML*/
	private transient String orginHtml;
	/**URL页面中包含的连接集合*/
	private transient List<Url> childUrlList;
	/**URL页面中包含的连接集合*/
	private transient List<CrawlLinkURI> childCrawlLinkURIList;
	/**路径*/
	private String path = "";
	public CrawlLinkURI() {
		super();
	}
	public CrawlLinkURI(Url url,String title, String type,String pathType) {
		super();
		this.url = url;
		this.title = title;
		this.type = type;
		this.pathType = pathType;
	}
	public Url getUrl() {
		return url;
	}
	public void setUrl(Url url) {
		this.url = url;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getPathType() {
		return pathType;
	}
	public void setPathType(String pathType) {
		this.pathType = pathType;
	}
	
	public CrawlURI getParentURI() {
		return parentURI;
	}
	public void setParentURI(CrawlURI parentURI) {
		this.parentURI = parentURI;
	}
	public String getHost() {
		if(StringUtils.isBlank(host) && null != this.parentURI){
			host = this.parentURI.getHost();
		}
		return host;
	}
	public void setHost(String host) {
		if(StringUtils.isBlank(host) && null != this.parentURI){
			host = this.parentURI.getHost();
		}
		this.host = host;
	}
	public int getPort() {
		if(0 == port && null != this.parentURI){
			port = this.parentURI.getPort();
		}
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	
	public String getRawPath() {
		return rawPath;
	}
	public void setRawPath(String rawPath) {
		this.rawPath = rawPath;
	}
	public boolean isSeed() {
		return isSeed;
	}
	public void setSeed(boolean isSeed) {
		this.isSeed = isSeed;
	}
	
	
	public CrawlResURI getResURI() {
		return resURI;
	}
	public void setResURI(CrawlResURI resURI) {
		this.resURI = resURI;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		if(null != url){
			return url.getUrl().substring(url.getUrl().lastIndexOf(File.separator), url.getUrl().length());
		}
		return "";
	}
	
	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}
	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	/**
	 * @return the depth
	 */
	public int getDepth() {
		return depth;
	}
	/**
	 * @param depth the depth to set
	 */
	public void setDepth(int depth) {
		this.depth = depth;
	}
	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}
	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	/**
	 * @return the tryNum
	 */
	public int getTryNum() {
		return tryNum;
	}
	/**
	 * @param tryNum the tryNum to set
	 */
	public void setTryNum(int tryNum) {
		this.tryNum = tryNum;
	}
	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @return the statusDesc
	 */
	public String getStatusDesc() {
		return statusDesc;
	}
	/**
	 * @param statusDesc the statusDesc to set
	 */
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}
	/**
	 * @param lastModified the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	/**
	 * @return the encoding
	 */
	public String getEncoding() {
		return encoding;
	}
	/**
	 * @param encoding the encoding to set
	 */
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
	/**
	 * @return the orginHtml
	 */
	public String getOrginHtml() {
		return orginHtml;
	}
	/**
	 * @param orginHtml the orginHtml to set
	 */
	public void setOrginHtml(String orginHtml) {
		this.orginHtml = orginHtml;
	}
	/**
	 * @return the childUrlList
	 */
	public List<Url> getChildUrlList() {
		return childUrlList;
	}
	/**
	 * @param childUrlList the childUrlList to set
	 */
	public void setChildUrlList(List<Url> childUrlList) {
		this.childUrlList = childUrlList;
	}
	
	/**
	 * @return the childCrawlLinkURIList
	 */
	public List<CrawlLinkURI> getChildCrawlLinkURIList() {
		return childCrawlLinkURIList;
	}
	/**
	 * @param childCrawlLinkURIList the childCrawlLinkURIList to set
	 */
	public void setChildCrawlLinkURIList(List<CrawlLinkURI> childCrawlLinkURIList) {
		this.childCrawlLinkURIList = childCrawlLinkURIList;
	}
	
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * 清理工作
	 */
	public void clear(){
		setParentURI(null);
		setTitle(null);
		setHost(null);
		setType(null);
		setPathType(null);
		setRawPath(null);
		setResURI(null);
		setOrginHtml(null);
		setChildUrlList(null);
		setChildCrawlLinkURIList(null);
		setUrl(null);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CrawlLinkURI other = (CrawlLinkURI) obj;
		if (url == null) {
			if (other.url != null){
				return false;
			}
			return true;	
		} else if (other.url == null){
				return false;
		}else if (!FilenameUtils.getName(url.getUrl()).equals(FilenameUtils.getName(other.url.getUrl()))){
			return false;
		}
			
		return true;
	}
	@Override
	public String toString() {
		return "Url [title=" + title + ", type=" + type + ", url=" + url + "]";
	}
	
	

}
