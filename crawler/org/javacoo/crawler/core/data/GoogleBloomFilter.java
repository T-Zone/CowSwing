/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.crawler.core.data;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnel;
import com.google.common.hash.Funnels;
import com.google.common.hash.PrimitiveSink;

/**
 * GoogleBloomFilter
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * 
 * @author DuanYong
 * @since 2015-2-3 下午8:58:24
 * @version 1.0
 */
public class GoogleBloomFilter {
	private int expectedInsertions = 2 << 24;
	private double fpp;
	private AtomicInteger counter;
	private final BloomFilter<CharSequence> bloomFilter;

	public GoogleBloomFilter() {
		this( 0.01);
	}

	public GoogleBloomFilter(double fpp) {
		this.fpp = fpp;
		this.bloomFilter = rebuildBloomFilter();
	}

	protected BloomFilter<CharSequence> rebuildBloomFilter() {
		counter = new AtomicInteger(0);
		return BloomFilter.create(Funnels.stringFunnel(Charset.defaultCharset()), expectedInsertions, fpp);
	}

	public void resetDuplicateCheck() {
		rebuildBloomFilter();
	}

	public int getTotalRequestsCount() {
		return counter.get();
	}

	public boolean isDuplicate(String url) {
		boolean isDuplicate = bloomFilter.mightContain(url);
		if (!isDuplicate) {
			bloomFilter.put(url);
			counter.incrementAndGet();
		}
		return isDuplicate;
	}

}
