package org.javacoo.crawler.core.processor.prepare;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.DateUtils;
import org.javacoo.crawler.core.constants.Constants;
import org.javacoo.crawler.core.data.Task;
import org.javacoo.crawler.core.data.uri.CrawlLinkURI;
import org.javacoo.crawler.core.data.uri.CrawlURI;
import org.javacoo.crawler.core.filter.Filter;
import org.javacoo.crawler.core.processor.AbstractProcessor;
import org.javacoo.crawler.core.util.HttpUrlHelper;

import com.javacoo.crawler.http.HttpResponse;

/**
 * 任务处理器接口-预处理链实现类
 * <li>预取链：主要是做一些准备工作，例如，对处理进行延迟和重新处理，否决随后的操作</li>
 * @author javacoo
 * @since 2011-11-09
 */
public class PrepareProcessor extends AbstractProcessor{

	public PrepareProcessor() {
		super();
	}

	@Override
	protected void innerProcess(Task task) {
		//如果有中间连接
		if(CollectionUtils.isNotEmpty(task.getController().getCrawlScope().getMidFilterList())){
			String tempHtml = null;
			CrawlLinkURI crawlLinkURI = task.getCrawlURI();
			log.info("=========取得中间连接---进入地址：========="+crawlLinkURI);
			for(Filter<String,Map<String, String>> fieldfilter : task.getController().getCrawlScope().getMidFilterList()){
				String field = null;
				List<CrawlLinkURI> tempLinkList =null;
				tempHtml = fetchHTML(task,crawlLinkURI);
				for(Iterator<String> it = fieldfilter.getFetchAreaTagMap().keySet().iterator(); it.hasNext();){
					field = it.next();
					log.info("=========取得中间连接========="+field);
				    // 取得、过滤指定属性/标签内容
					tempLinkList = task.getController().getHtmlParserWrapper().getCrawlLinkURIByFilterMap(tempHtml,fieldfilter.getFetchAreaTagMap().get(field),fieldfilter.getDeleteAreaTagMap().get(field),fieldfilter.getContentHandleMap().get(field),crawlLinkURI);
					if(CollectionUtils.isNotEmpty(tempLinkList)){
						crawlLinkURI = tempLinkList.get(0);
						log.info("=========取得中间连接地址：========="+crawlLinkURI);
					}
				}
			}
			log.info("=========取得中间连接---结果地址：========="+crawlLinkURI);
			task.setCrawlURI(crawlLinkURI);
		}
		//初始化任务
		prepareTask(task);
	}
	
	/**
	 * 根据任务URL地址,取得HTML
	 * @param task 任务
	 */
	private String fetchHTML(Task task,CrawlURI crawlURI){
		log.info("=========下载网页,提取原始html=========");
		HttpResponse rsp = task.getController().getHttpHandler().getSync(HttpUrlHelper.getRawUrl(crawlURI),HttpUrlHelper.getHeaderInfo());
		log.info("=========HTML内容=========");
		log.info(rsp.getContent());
		return rsp.getContent();
	}
     /**
      * 任务准备
      * <p>方法说明:</>
      * <li>取得页面编码等信息</li>
      * @author DuanYong
      * @since 2015-3-1 上午10:56:44
      * @version 1.0
      * @exception 
      * @param task
      */
	private void prepareTask(Task task){
		String allPath = "";
		try {
			allPath = HttpUrlHelper.getHttpGetUrl(task);
			if(StringUtils.isBlank(allPath)){
				return;
			}
            //设置全路径
			task.getCrawlURI().getUrl().setUrl(allPath);
			task.getCrawlURI().getUrl().setPathType(Constants.PATH_TYPE_0);
			HttpResponse rsp = task.getController().getHttpHandler().getSync(allPath,HttpUrlHelper.getHeaderInfo());
			String statusCode = String.valueOf(rsp.getStatusCode());
			task.getCrawlURI().setStatusCode(statusCode);
			task.getCrawlURI().setStatusDesc(Constants.HTTP_STATUS_CODE_MAP.get(statusCode));
			if(StringUtils.isNotBlank(rsp.getLastModified())){
				Date lastModified = DateUtils.parseDate(rsp.getLastModified());
				task.getCrawlURI().setLastModified(lastModified);
			}
			if(StringUtils.isNotBlank(rsp.getCharset())){
				task.getCrawlURI().setEncoding(rsp.getCharset());
			}
		}catch (Exception e) {
			task.getCrawlURI().setStatusCode(Constants.HTTP_STATUS_CODE_001);
			task.getCrawlURI().setStatusDesc(Constants.HTTP_STATUS_CODE_MAP.get(Constants.HTTP_STATUS_CODE_001));
            if(task.getCrawlURI().getTryNum() < Constants.TRY_MAX_NUM ){
			    task.getCrawlURI().setSuccess(false);
            	task.getCrawlURI().setTryNum(task.getCrawlURI().getTryNum()+1);
    			task.getController().getFrontier().addCrawlLinkURIToWait(task.getCrawlURI());
            	log.error("访问失败，重试："+task.getCrawlURI().getTryNum()+"次");
            }
			e.printStackTrace();
		} 
	}
	
}
