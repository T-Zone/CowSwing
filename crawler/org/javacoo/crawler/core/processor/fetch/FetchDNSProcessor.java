package org.javacoo.crawler.core.processor.fetch;


import org.javacoo.crawler.core.data.Task;
import org.javacoo.crawler.core.processor.AbstractProcessor;
import org.javacoo.crawler.core.util.DynamicPageUtil;
import org.javacoo.crawler.core.util.HttpUrlHelper;

import com.javacoo.crawler.http.HttpResponse;


/**
 * 任务处理器接口-提取DNS实现类
 * <li>提取链：主要是下载网页，进行 DNS 转换，填写请求和响应表单</li>
 * @author javacoo
 * @since 2011-11-09
 */
public class FetchDNSProcessor extends AbstractProcessor{

	public FetchDNSProcessor() {
		super();
	}

	@Override
	protected void innerProcess(Task task) {
		if(task.getCrawlURI().isSeed() && task.getController().getCrawlScope().isDynamicParserFlag()){
			fetchDynamicHtml(task);
		}else{
			fetchNormalHtml(task);
		}
	}
	/**
	 * 下载动态网页
	 * <p>说明:</p>
	 * <li></li>
	 * @param task
	 * @since 2017年12月13日下午3:57:15
	 */
	private void fetchDynamicHtml(Task task){
		log.info("=========下载动态网页,提取原始html=========");
		String html = DynamicPageUtil.getHtml(task.getCrawlURI().getUrl().getUrl());
		task.getContentBean().setOrginHtml(html);
		log.info("=========动态HTML内容=========");
		log.info(html);
	}
	/**
	 * 下载普通网页
	 * <p>说明:</p>
	 * <li></li>
	 * @param task
	 * @since 2017年12月13日下午3:57:34
	 */
	private void fetchNormalHtml(Task task){
		log.info("=========下载普通网页,提取原始html=========");
		try {
			HttpResponse rsp = task.getController().getHttpHandler().getSync(HttpUrlHelper.getHttpGetUrl(task),HttpUrlHelper.getHeaderInfo());
			task.getContentBean().setOrginHtml(rsp.getContent());
			log.info("=========HTML内容=========");
			log.info(rsp.getContent());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
