package org.javacoo.crawler.core.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.javacoo.crawler.core.data.SimpleBloomFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * URL缓存
 * <li>用于去重</li>
 * @author DuanYong 
 * @since 2015-1-21下午8:38:53
 * @version 1.0
 */
public class BloomFilterUrlCache implements UrlCache{
	protected final Logger log =  LoggerFactory.getLogger(BloomFilterUrlCache.class);
	/***
	 * 缓存
	 */
	private static Map<Integer,SimpleBloomFilter<String>> cache = new ConcurrentHashMap<>();
	

	/**
	 * 检查URL
	 * <li></li>
	 * @author DuanYong 
	 * @since 2015-1-21下午8:43:16
	 * @version 1.0
	 * @param url
	 * @param ruleId
	 * @return
	 */
	@Override
	public boolean checkUrlIsExist(String url,Integer ruleId){
		if(StringUtils.isBlank(url)){
			return true;
		}
		SimpleBloomFilter<String> b = cache.get(ruleId);
		if(null != b){
			if(b.contains(url)){
				return true;
			}
		}else{
			b = new SimpleBloomFilter<String>();
			cache.put(ruleId,b);
		}
		b.add(url);
		log.info("URL缓存：规则ID={},大小={}",ruleId,b.getSize());
		return false;
	}
	/**
	 * 清除
	 * <li></li>
	 * @author DuanYong 
	 * @since 2015-1-21下午8:54:25
	 * @version 1.0
	 */
    @Override
	public void clear(){
		cache.clear();
	}
	/**
	 * 根据规则ID清除
	 * <li></li>
	 * @author DuanYong 
	 * @since 2015-1-21下午9:14:15
	 * @version 1.0
	 * @param ruleId
	 */
    @Override
	public void clearById(Integer ruleId){
		cache.remove(ruleId);
	}
}
