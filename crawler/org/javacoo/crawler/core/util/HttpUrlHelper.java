package org.javacoo.crawler.core.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.methods.HttpGet;
import org.javacoo.cowswing.core.constant.Config;
import org.javacoo.crawler.core.constants.Constants;
import org.javacoo.crawler.core.data.Task;
import org.javacoo.crawler.core.data.uri.CrawlURI;

import com.javacoo.crawler.http.HeaderInfo;
/**
 * HttpUrlHelper
 * @author javacoo
 * @since 2011-12-17
 * @modify 2012-05-02
 */
public class HttpUrlHelper{
	private static Log log =  LogFactory.getLog(HttpUrlHelper.class);
	public static String getHttpGetUrl(Task task) throws URISyntaxException{
		String url = StringUtils.trim(task.getCrawlURI().getUrl().getUrl());
		String pathType = task.getCrawlURI().getUrl().getPathType();
		URI uri = new URI(url);
		String host = uri.getHost();
		//取得当前URL的路径
		String path = getPath(uri);
		int port = uri.getPort();
		if(Constants.PATH_TYPE_1.equals(pathType)){
			host = task.getController().getCrawlScope().getDefaultHost();
			port = task.getController().getCrawlScope().getDefaultPort();
			if(StringUtils.isBlank(path)){
				path = task.getController().getCrawlScope().getDefaultPath();
				uri = populateUri(url,host,port,path);
			}else{
				//相对根路径，则直接是默认主机+端口+URL
				uri = populateUri(url,host,port,"");
			}
			
		}else if(Constants.PATH_TYPE_2.equals(pathType)){
			if(null != task.getCrawlURI().getParentURI().getHost()){
				host = task.getCrawlURI().getParentURI().getHost();
				port = task.getCrawlURI().getParentURI().getPort();
				if(StringUtils.isBlank(path)){
					path = task.getCrawlURI().getParentURI().getPath();
				}else{
					path = "";
				}
			}else{
				host = task.getController().getCrawlScope().getDefaultHost();
				port = task.getController().getCrawlScope().getDefaultPort();
				path = task.getController().getCrawlScope().getDefaultPath();
			}
			uri = populateUri(url,host,port,path);
		}
		task.getCrawlURI().setPathType(pathType);
		task.getCrawlURI().setPath(path);
		task.getCrawlURI().setHost(host);
		task.getCrawlURI().setPort(port);
		if(!OtherUtils.checkHost(task)){
			task.finished();
			return null;
		}
		return uri.toString();
	} 
	/**
	 * 获取请求头设置
	 * <p>说明:</p>
	 * <li></li>
	 * @return
	 * @since 2018年2月8日下午4:21:31
	 */
	public static HeaderInfo getHeaderInfo(){
		HeaderInfo headerInfo = new HeaderInfo();
		Map<String,String> headerMap = new HashMap<>();
		headerMap.put("Accept",Config.CRAWLER_CONFIG_MAP.get(Config.CRAWLER_CONFIG_KEY_CORE).get(Config.CRAWLER_CONFIG_KEY_CORE_ACCEPT)); 
		headerMap.put("Accept-Language",Config.CRAWLER_CONFIG_MAP.get(Config.CRAWLER_CONFIG_KEY_CORE).get(Config.CRAWLER_CONFIG_KEY_CORE_ACCEPT_LANGUAGE));
		headerMap.put("User-Agent", Config.CRAWLER_CONFIG_MAP.get(Config.CRAWLER_CONFIG_KEY_CORE).get(Config.CRAWLER_CONFIG_KEY_CORE_USER_AGENT));
		headerMap.put("Accept-Charset",Config.CRAWLER_CONFIG_MAP.get(Config.CRAWLER_CONFIG_KEY_CORE).get(Config.CRAWLER_CONFIG_KEY_CORE_ACCEPT_CHARSET));
		headerMap.put("Keep-Alive", Config.CRAWLER_CONFIG_MAP.get(Config.CRAWLER_CONFIG_KEY_CORE).get(Config.CRAWLER_CONFIG_KEY_CORE_KEEP_ALIVE));
		headerMap.put("Connection", Config.CRAWLER_CONFIG_MAP.get(Config.CRAWLER_CONFIG_KEY_CORE).get(Config.CRAWLER_CONFIG_KEY_CORE_CONNECTION));
		headerMap.put("Cache-Control", Config.CRAWLER_CONFIG_MAP.get(Config.CRAWLER_CONFIG_KEY_CORE).get(Config.CRAWLER_CONFIG_KEY_CORE_CACHE_CONTROL)); 
		headerInfo.setHeaderMap(headerMap);
		return headerInfo;
	}
	private static URI populateUri(String url,String host,int port,String path) throws URISyntaxException{
		String tempUrl = "";
		if(port != -1){
			tempUrl = Constants.HTTP_FILL_KEY+host+":"+port;
		}else{
			tempUrl = Constants.HTTP_FILL_KEY+host;
		}
		return new URI(tempUrl+path+url);
	}
	private static String getPath(URI uri){
		String path = uri.toString();
		if(null == path || StringUtils.isBlank(path) || path.lastIndexOf("/") == path.indexOf("/")){
			return "";
		}
//		else {
//			int endPos = path.lastIndexOf("/");
//			path = path.substring(0, endPos);
//		}
		return path;
	}
	/**
	 * 获取请求路径
	 * <p>说明:</p>
	 * <li></li>
	 * @param uri
	 * @return
	 * @since 2018年2月8日下午4:21:46
	 */
	public static String getRawUrl(CrawlURI uri){
		Log log =  LogFactory.getLog(HttpGet.class);
		String pathType = uri.getPathType();
		//取得当前URL的路径
		String rawPath = uri.getRawPath();
		//绝对路径
		if(Constants.PATH_TYPE_2.equals(pathType)){
			//如果是相对当前路径，则取得父路径的RawPath在加上当前RawPath
			if(null != uri.getParentURI()){
				rawPath = getRawPath(uri.getParentURI().getRawPath())+rawPath;
			}
		}
		log.info("==============当前访问路径========"+rawPath);
		return rawPath;
		
	}
	/**
	 * 取得URI对象的路径
	 * @param uri
	 * @return
	 */
	private static String getRawPath(String rawPath){
		if(StringUtils.isBlank(rawPath) || rawPath.lastIndexOf("/") == rawPath.indexOf("/")){
			rawPath = "";
		}else {
			int endPos = rawPath.lastIndexOf("/");
			rawPath = rawPath.substring(0, endPos);
		}
		return rawPath;
	}
	public static void main(String[] args) throws URISyntaxException{
		String url = "index.html";
		String tempUrl = "/chufang/diy/jiangchangcaipu/?&page=4";
		URI uri = new URI(tempUrl);
		String host = uri.getHost();
		//取得当前URL的路径
		String path = uri.toString();
		//String path = uri.getRawPath();
		String query = uri.getRawQuery();
		int port = uri.getPort();
//		if(StringUtils.isBlank(path) || path.lastIndexOf("/") == path.indexOf("/")){
//			path = "";
//		}else {
//			int endPos = path.lastIndexOf("/");
//			path = path.substring(0, endPos);
//		}
		System.out.println(path);
		System.out.println(query);
		System.out.println(host);
		System.out.println(port);
		
	}

}
