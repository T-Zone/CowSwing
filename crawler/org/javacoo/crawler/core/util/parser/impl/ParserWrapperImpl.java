package org.javacoo.crawler.core.util.parser.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.htmlparser.Node;
import org.htmlparser.Parser;
import org.htmlparser.PrototypicalNodeFactory;
import org.htmlparser.filters.NodeClassFilter;
import org.htmlparser.filters.OrFilter;
import org.htmlparser.tags.FrameTag;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;
import org.javacoo.crawler.core.constants.Constants;
import org.javacoo.crawler.core.data.Task;
import org.javacoo.crawler.core.data.Url;
import org.javacoo.crawler.core.data.uri.CrawlLinkURI;
import org.javacoo.crawler.core.data.uri.CrawlResURI;
import org.javacoo.crawler.core.data.uri.CrawlURI;
import org.javacoo.crawler.core.filter.Filter;
import org.javacoo.crawler.core.filter.factory.FilterFactory;
import org.javacoo.crawler.core.util.URIHelper;
import org.javacoo.crawler.core.util.parser.HtmlParser;
import org.javacoo.crawler.core.util.parser.ParserWrapper;
import org.javacoo.crawler.core.util.parser.tag.EmbedTag;
import org.javacoo.crawler.core.util.parser.tag.IFrameTag;

/**
 * HTML解析包装类接口实现类
 * @author javacoo
 * @since 2012-05-12
 */
@SuppressWarnings("unchecked")
public class ParserWrapperImpl implements ParserWrapper{
	private static Log log =  LogFactory.getLog(ParserWrapperImpl.class);
	/**过滤器工厂*/
	private FilterFactory filterFactory;
	private PrototypicalNodeFactory factory;
	/**	HTML解析工具类*/
	private HtmlParser htmlParser;
	/**	regex解析工具类*/
	private org.javacoo.crawler.core.util.parser.Parser regexParser;
	/**	xpath解析工具类*/
	private org.javacoo.crawler.core.util.parser.Parser xpathParser;
	/**
	 * HTML解析包装类构造方法
	 * @param filterFactory 过滤器工
	 */
	public ParserWrapperImpl(FilterFactory filterFactory, URIHelper uriHelper){
		this.filterFactory = filterFactory;
		//this.htmlParser = new HtmlParserImpl(uriHelper);
		this.htmlParser = new JsoupSelectorImpl(uriHelper);
		this.regexParser = new RegexParserImpl();
	    this.xpathParser = new XpathParserImpl();
		factory = new PrototypicalNodeFactory();  
		factory.registerTag(new EmbedTag());  
		factory.registerTag(new IFrameTag());  
	}
	
	/**
	 * 取得文章列表区域链接集合
	 * @param orginHtml 原始HTML
	 * @param parentCrawlURI 父crawlURI
	 * @return 标题集合
	 */
	public List<CrawlLinkURI> getLinkAreaUrlList(String orginHtml,CrawlURI parentCrawlURI) {
		Filter<String,String> linkAreafilter = filterFactory.getFilterByName(Constants.FILTER_NAME_LINK_AREA);
		List<CrawlLinkURI> linkAreaList = new ArrayList<CrawlLinkURI>();
		if(!org.javacoo.crawler.core.util.CollectionUtils.isEmpty(linkAreafilter.getFetchAreaTagMap())){
			linkAreaList = htmlParser.getUrlList(orginHtml, linkAreafilter.getFetchAreaTagMap(), linkAreafilter.getDeleteAreaTagMap(),linkAreafilter.getContentHandleMap(),parentCrawlURI);
		}
		return linkAreaList;
	}
	

	/**
	 * 取得原始HTML中指定条件的值并放在对应字段
	 * @param orginHtml 原始HTML
	 * @return 字段 值对照MAP
	 */
	public Map<String,String> getFieldValues(String orginHtml){
		Map<String,String> resultMap = new HashMap<String, String>();
		List<Filter<String,Map<String, String>>> fieldfilterList = filterFactory.getFilterListByName(Constants.FILTER_NAME_FIELD);
		if(CollectionUtils.isNotEmpty(fieldfilterList)){
			for(Filter<String,Map<String, String>> fieldfilter : fieldfilterList){
				String field = null;
				String tempHtml = null;
				String optHtml = "";
				StringBuilder tempValue = null;
				List<String> tempList = null;
				for(Iterator<String> it = fieldfilter.getFetchAreaTagMap().keySet().iterator(); it.hasNext();){
					field = it.next();
					optHtml = new String(orginHtml);
					//XPATH解析
					if(Constants.FILTER_TYPE_XPATH.equals(fieldfilter.getFilterType())){
						tempList = xpathParser.getList(orginHtml, fieldfilter.getFetchAreaTagMap().get(field).get(Constants.OTHER_TAG));
					}else if(Constants.FILTER_TYPE_REGEX.equals(fieldfilter.getFilterType())){//正则表达式
						tempList = regexParser.getList(orginHtml, fieldfilter.getFetchAreaTagMap().get(field).get(Constants.OTHER_TAG));
					}else{
						// 取得、过滤指定属性/标签内容
						tempHtml = htmlParser.getHtml(optHtml,fieldfilter.getFetchAreaTagMap().get(field),fieldfilter.getDeleteAreaTagMap().get(field),fieldfilter.getContentHandleMap().get(field));
						tempList = htmlParser.getPlainTextList(tempHtml, fieldfilter.getFetchAreaTagMap().get(field),fieldfilter.getContentHandleMap().get(field));
					}
					if(!CollectionUtils.isEmpty(tempList)){
						tempValue = new StringBuilder();
						for(String str : tempList){
							tempValue.append(str).append(Constants.EXTEND_SPLIT_KEY);
						}
						resultMap.put(field,tempValue.substring(0, tempValue.length() - 1).toString());
					}
				}
			}
		}
		return resultMap;
	}
	/**
     * 取得指定区域的HTML内容
     * @param orginHtml 原始HTML
     * @return 指定区域的HTML内容
     * @throws ParserException
     */
	@Override
	public String getContentHtml(String orginHtml) {
		Filter<String,String> contentAreafilter = filterFactory.getFilterByName(Constants.FILTER_NAME_CONTENT_AREA);
		return htmlParser.getHtml(orginHtml, contentAreafilter.getFetchAreaTagMap(), contentAreafilter.getDeleteAreaTagMap(),contentAreafilter.getContentHandleMap());
	}
	/**
     * 取得指定区域的文章HTML内容,未过滤
     * @return 指定区域的HTML内容
     */
    @Override
	public String getTargetContentHtml(String orginHtml){
		Filter<String,String> contentAreafilter = filterFactory.getFilterByName(Constants.FILTER_NAME_CONTENT_AREA);
		return htmlParser.fetchAreaHtml(orginHtml, contentAreafilter.getFetchAreaTagMap());
	}
	/**
     * 取得指定区域的文章HTML内容,未过滤
     * @return 指定区域的HTML内容
     */
    @Override
	public List<String> getTargetContentHtmlForList(String orginHtml){
		Filter<String,String> contentAreafilter = filterFactory.getFilterByName(Constants.FILTER_NAME_LINK_AREA);
		return htmlParser.fetchAreaHtmlForList(orginHtml, contentAreafilter.getFetchAreaTagMap());
	}
	/**
     * 过滤指定区域的文章HTML内容
     * @return 过滤后的HTML内容
     */
    @Override
	public String filterTargetContentHtml(String orginHtml){
		Filter<String,String> contentAreafilter = filterFactory.getFilterByName(Constants.FILTER_NAME_CONTENT_AREA);
		return htmlParser.deleteAreaHtml(orginHtml, contentAreafilter.getDeleteAreaTagMap());
	}
	/**
	 * 取得无格式的HTML内容
	 * @param orginHtml 原始HTML内容
	 * @return 无格式的HTML内容
	 */
    @Override
	public String getPlainText(String orginHtml){
		return htmlParser.getPlainText(orginHtml);
	}
	/**
	 * 取得HTML内容中的资源
	 * @param orginHtml 原始HTML内容
	 * @param resCrawlURIList 资源URI对象集合
	 * @param parentCrawlURI 父crawlURI
	 * @return String
	 */
    @Override
	public String getHtmlResource(String orginHtml,List<CrawlResURI> resCrawlURIList,CrawlURI parentCrawlURI){
		return htmlParser.getHtmlResource(orginHtml, resCrawlURIList, parentCrawlURI);
		
	}
	/**
     * 取得指定HTML内容中的摘要信息
     * @param orginHtml 原始HTML信息
     * @return 指定区域的HTML内容
     */
    @Override
	public String getContentBrief(String orginHtml) {
		log.info("========取得指定HTML内容中的摘要信息=========");
		String returnHtml = "";
		if (StringUtils.isNotBlank(orginHtml)) {
			Filter<String,String> briefAreafilter = filterFactory.getFilterByName(Constants.FILTER_NAME_BRIEF_AREA);
			if(org.javacoo.crawler.core.util.CollectionUtils.isEmpty(briefAreafilter.getFetchAreaTagMap())){//如果摘要采集参数为空,则提取内容中指定字数字符作为摘要信息
				returnHtml = getTargetContentHtml(orginHtml);
				returnHtml = filterTargetContentHtml(returnHtml);
				returnHtml = getPlainText(returnHtml);
			}else{
				returnHtml = htmlParser.getHtml(orginHtml, briefAreafilter.getFetchAreaTagMap(), briefAreafilter.getDeleteAreaTagMap(),briefAreafilter.getContentHandleMap());
				returnHtml = getPlainText(returnHtml);
			}
			returnHtml = StringUtils.left(returnHtml, Constants.BRIEF_NUM);
		}
		return returnHtml;
	}
	/**
     * 取得指定HTML内容中的分页链接列表信息
     * @param orginHtml 原始HTML信息
	 * @param parentCrawlURI 父crawlURI
     * @return 分页链接列表
     */
    @Override
	public List<CrawlLinkURI> getPaginationList(String orginHtml,CrawlURI parentCrawlURI) {
		log.info("========取得指定HTML内容中的分页链接列表信息=========");
		Filter<String,String> paginationAreafilter = filterFactory.getFilterByName(Constants.FILTER_NAME_PAGINATION_AREA);
		List<CrawlLinkURI> paginationList = new ArrayList<CrawlLinkURI>();
		if(!org.javacoo.crawler.core.util.CollectionUtils.isEmpty(paginationAreafilter.getFetchAreaTagMap())){
			paginationList = htmlParser.getUrlList(orginHtml,paginationAreafilter.getFetchAreaTagMap(), paginationAreafilter.getDeleteAreaTagMap(),paginationAreafilter.getContentHandleMap(),parentCrawlURI);
		}
		return  paginationList;
	}
	/**
     * 取得指定HTML内容中的评论列表入口URL
     * @param orginHtml 原始HTML信息
	 * @param parentCrawlURI 父crawlURI
     * @return Url
     */
    @Override
	public CrawlLinkURI getCommentIndexUrl(String orginHtml,CrawlURI parentCrawlURI){
		Filter<String,String> commentIndexAreafilter = filterFactory.getFilterByName(Constants.FILTER_NAME_COMMENT_INDEX_AREA);
		if(!org.javacoo.crawler.core.util.CollectionUtils.isEmpty(commentIndexAreafilter.getFetchAreaTagMap())){
			List<CrawlLinkURI> urlList =  htmlParser.getUrlList(orginHtml,commentIndexAreafilter.getFetchAreaTagMap(), commentIndexAreafilter.getDeleteAreaTagMap(),commentIndexAreafilter.getContentHandleMap(),parentCrawlURI);
			if(!CollectionUtils.isEmpty(urlList)){
				return urlList.get(0);
			}
		}
		return null;
	}
	/**
     * 取得指定HTML内容中的评论内容列表区域HTML内容
     * @param orginHtml 原始HTML信息
     * @return String 评论内容列表区域HTML内容
     */
    @Override
	public String getCommentListArea(String orginHtml){
		Filter<String,String> commentListAreafilter = filterFactory.getFilterByName(Constants.FILTER_NAME_COMMENT_LIST_AREA);
		return htmlParser.getHtml(orginHtml, commentListAreafilter.getFetchAreaTagMap(), commentListAreafilter.getDeleteAreaTagMap(),commentListAreafilter.getContentHandleMap());
	}
	/**
     * 取得指定HTML内容中的评论链接列表信息
     * @param orginHtml 原始HTML信息
	 * @param parentCrawlURI 父crawlURI
     * @return 评论链接列表
     */
    @Override
	public List<CrawlLinkURI> getCommentUrlList(String orginHtml,CrawlURI parentCrawlURI) {
		log.info("========取得指定HTML内容中的评论链接列表信息=========");
		Filter<String,String> commentLinkAreafilter = filterFactory.getFilterByName(Constants.FILTER_NAME_COMMENT_LINK_AREA);
		List<CrawlLinkURI> commentLinkList = new ArrayList<CrawlLinkURI>();
		if(!org.javacoo.crawler.core.util.CollectionUtils.isEmpty(commentLinkAreafilter.getFetchAreaTagMap())){
			commentLinkList = htmlParser.getUrlList(orginHtml,commentLinkAreafilter.getFetchAreaTagMap(), commentLinkAreafilter.getDeleteAreaTagMap(),commentLinkAreafilter.getContentHandleMap(),parentCrawlURI);
		}
		return commentLinkList;
	}
	/**
	 * 取得评论内容集合
	 * @param orginHtml
	 * @return
	 */
    @Override
	public List<String> getCommentList(String orginHtml){
		Filter<String,String> commentfilter = filterFactory.getFilterByName(Constants.FILTER_NAME_COMMENT_AREA);
		return htmlParser.getHtmlList(orginHtml, commentfilter.getFetchAreaTagMap());
	}
	/**
     * 过滤评论内容区域的HTML内容
     * @return 过滤的HTML内容
     */
    @Override
	public String filterCommentHtml(String orginHtml){
		log.info("========过滤评论内容区域的HTML内容=========");
		Filter<String,String> commentfilter = filterFactory.getFilterByName(Constants.FILTER_NAME_COMMENT_AREA);
		return htmlParser.deleteAreaHtml(orginHtml, commentfilter.getDeleteAreaTagMap());
		
	}
	/**
	 * 替换HTML内容中资源链接地址为指定地址，并返回包含了替换后HTML，原始资源链接与替换后资源链接对照的MAP对象
	 * @param orginHtml 原始HTML内容
	 * @param task 任务对象
	 * @return string
	 */
    @Override
	public String replaceHtmlResource(String orginHtml,Task task){
		return htmlParser.replaceHtmlResource(orginHtml,task);
	}
	/**
	 * 替换内容中的超链接
	 * @param orginHtml 原始HTML内容
	 * @return 去掉超链接后的HTML
	 */
    @Override
	public String replaceHtmlLink(String orginHtml){
		return htmlParser.replaceHtmlLink(orginHtml);
	}
	/**
	 * 取得连CrawlURI集合，如果有标题图片则放到资源MAP中
	 * @param task 任务对象
	 * @return CrawlURI集合
	 */
    @Override
	public List<CrawlLinkURI> getCrawlURIList(Task task){
		Filter<String,String> linkfilter = filterFactory.getFilterByName(Constants.FILTER_NAME_LINK_AREA);
		return htmlParser.getCrawlURIList(task, linkfilter.getFetchAreaTagMap(), linkfilter.getDeleteAreaTagMap(),linkfilter.getContentHandleMap());
	}
	/**
     * 取得指定区域,过滤标签的HTML内容
     * @param fetchAreaTagMap 待提取区域标签属性Map
	 * @param deleteAreaTagMap 待提取区域中要删除的标签属性Map
	 * @param parentCrawlURI 父crawlURI
     * @return 指定区域的连接内容
     */
    @Override
	public List<CrawlLinkURI> getCrawlLinkURIByFilterMap(String orginHtml,Map<String,String> fetchAreaTagMap,Map<String,String> deleteAreaTagMap,Map<String,String> contentHandleMap,CrawlURI parentCrawlURI) {
		List<CrawlLinkURI> commentLinkList = new ArrayList<CrawlLinkURI>();
		if(!org.javacoo.crawler.core.util.CollectionUtils.isEmpty(fetchAreaTagMap)){
			commentLinkList = htmlParser.getUrlList(orginHtml,fetchAreaTagMap, deleteAreaTagMap,contentHandleMap,parentCrawlURI);
		}
		return commentLinkList;
	}
    @Override
	public List<Url> getUrlList(String orginHtml){
		//网页连接URL
		List<Url> urlList = new CopyOnWriteArrayList<Url>();
		if(StringUtils.isNotBlank(orginHtml)){
			try {
				Parser parser = new Parser();
				parser.setNodeFactory(factory);
				parser.setInputHTML(orginHtml);
				OrFilter linkOrFrameFilter = new OrFilter(new NodeClassFilter(LinkTag.class), new NodeClassFilter(FrameTag.class));
				OrFilter imageOrEmbedFilter = new OrFilter(new NodeClassFilter(EmbedTag.class), new NodeClassFilter(ImageTag.class));
				OrFilter tempFilter =  new OrFilter(linkOrFrameFilter, imageOrEmbedFilter);
				OrFilter orFilter =  new OrFilter(new NodeClassFilter(IFrameTag.class), tempFilter);
				NodeList nodes = parser.extractAllNodesThatMatch(orFilter);
				Matcher m = null;
				for (int i = 0; i < nodes.size(); i++) {
					Node tag = nodes.elementAt(i);
					if(tag instanceof LinkTag){
						LinkTag link = (LinkTag) tag;
						//如果连接是 js链接,邮件链接,FTP链接,IRC连接，则不予处理
						if(link.isJavascriptLink() || link.isMailLink() || link.isFTPLink() || link.isIRCLink()){
							continue;
						}
						if(link.getLink().contains(".jpg") 
								|| link.getLink().contains(".gif") 
								|| link.getLink().contains(".png")
								|| link.getLink().contains(".bmp")){
							populateList(urlList,link.getLink(),StringUtils.trim(link.getLinkText()),Constants.URL_TYPE_RES);
						}else{
							//log.info("=======================取得LinkTag"+link.getLink());
							populateList(urlList,link.getLink(),StringUtils.trim(link.getLinkText()),Constants.URL_TYPE_LINK);
						}
					}else if (tag instanceof FrameTag){
						FrameTag frame = (FrameTag) tag;
						//log.info("=======================取得FrameTag"+frame.getFrameLocation());
						populateList(urlList,frame.getFrameLocation(),"",Constants.URL_TYPE_RES);
					}else if (tag instanceof IFrameTag){
						IFrameTag frame = (IFrameTag) tag;
						//log.info("=======================取得FrameTag"+frame.getFrameLocation());
						populateList(urlList,frame.getIFrameLocation(),"",Constants.URL_TYPE_LINK);
					}else if (tag instanceof ImageTag){
						ImageTag image = (ImageTag) tag;
						//log.info("=======================取得ImageTag"+image.getImageURL());
						populateList(urlList,image.getImageURL(),"",Constants.URL_TYPE_RES);
					}else if (tag instanceof EmbedTag){
						EmbedTag embed = (EmbedTag) tag;
						//log.info("=======================取得EmbedTag"+embed.getSrc());
						populateList(urlList,embed.getSrc(),"",Constants.URL_TYPE_RES);
					}
				}
			} catch (ParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return urlList;
		
	}
	/**
	 * 组装LIST
	 * @param list 目标LIST
	 */
	private void populateList(List<Url> list,String urlStr,String title,String type){
		urlStr = StringUtils.trim(urlStr);
		if(StringUtils.isNotBlank(urlStr)){
			String pathType = Constants.PATH_TYPE_0;
			//空链接
			if(urlStr.endsWith(Constants.EMPTY_LINK_STR)){
				return;
			}
			//相对当前路径
			if(!urlStr.startsWith("http://") && !urlStr.startsWith("https://") && !urlStr.startsWith("/")){
				urlStr = "/"+urlStr;
				pathType = Constants.PATH_TYPE_2;
			}else if(!urlStr.startsWith("http://") && !urlStr.startsWith("https://") && urlStr.startsWith("/")){
				pathType = Constants.PATH_TYPE_1;
			}
			Url url = new Url(urlStr,title,type,pathType);
			if(!list.contains(url)){
				list.add(url);
			}
		}
	}
	public static void main(String[] args) throws ParserException,
			URISyntaxException, IOException {
		
	}

}
