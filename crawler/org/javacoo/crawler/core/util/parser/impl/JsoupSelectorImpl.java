package org.javacoo.crawler.core.util.parser.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.queryparser.xml.ParserException;
import org.javacoo.crawler.core.constants.Constants;
import org.javacoo.crawler.core.data.Task;
import org.javacoo.crawler.core.data.uri.CrawlLinkURI;
import org.javacoo.crawler.core.data.uri.CrawlResURI;
import org.javacoo.crawler.core.data.uri.CrawlURI;
import org.javacoo.crawler.core.thread.swaparea.SwapAreaUtils;
import org.javacoo.crawler.core.util.HttpUrlHelper;
import org.javacoo.crawler.core.util.URIHelper;
import org.javacoo.crawler.core.util.generator.DefaultGenerator;
import org.javacoo.crawler.core.util.generator.Generator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import com.javacoo.crawler.http.HttpResponse;

/**
 * HTML解析工具,jsoup实现类
 * <p>
 * 说明:
 * </p>
 * <li>采集参数支持JSOUP选择器语法</li>
 * @author DuanYong
 * @since 2015-9-22下午8:42:25
 * @version 1.0
 */
public class JsoupSelectorImpl extends AbstractHtmlParser{
	private static Log log =  LogFactory.getLog(JsoupSelectorImpl.class);
	/**唯一标示生成接口,默认实现*/
	private Generator generator = new DefaultGenerator();
	private URIHelper uriHelper;
	/**
	 * HTML解析类构造方法
	 * @param uriHelper
	 */
	public JsoupSelectorImpl(URIHelper uriHelper){
		this.uriHelper = uriHelper;
	}
	
	/**
	 * 取得指定区域的连接集合
	 * @param orginHtml 原始HTML
	 * @param fetchAreaTagMap 待提取区域标签属性Map
	 * @param deleteAreaTagMap 待提取区域中要删除的标签属性Map
	 * @param contentHandleMap 内容处理Map
	 * @param parentCrawlURI 父crawlURI
	 * @return 连接集合
	 */
	@Override
	public List<CrawlLinkURI> getUrlList(String orginHtml,Map<String,String> fetchAreaTagMap,Map<String,String> deleteAreaTagMap,Map<String,String> contentHandleMap,CrawlURI parentCrawlURI) {
		orginHtml = getHtmlByFilter(fetchAreaTagMap, deleteAreaTagMap,contentHandleMap,orginHtml);
		return getUrlListFromOrginHtml(orginHtml,parentCrawlURI);
	}
	/**
	 * 取得指定区域内的所有连接,并组装为list
	 * @param orginHtml 原始HTML
	 * @param parentCrawlURI 父crawlURI
	 * @return 连接集合
	 */
	private List<CrawlLinkURI> getUrlListFromOrginHtml(String orginHtml,CrawlURI parentCrawlURI){
		List<CrawlLinkURI> resultList = new CopyOnWriteArrayList<CrawlLinkURI>();
		if (StringUtils.isNotBlank(orginHtml)) {
			try {
				Document doc = Jsoup.parse(orginHtml);
				Elements links = doc.getElementsByTag(Constants.TAG_LINK);
				for(Element link : links){
					if(StringUtils.isNotBlank(link.attr(Constants.TAG_LINK_ATTR_HREF)) && 
							!Constants.EMPTY_LINK_STR.equals(StringUtils.trim(link.attr(Constants.TAG_LINK_ATTR_HREF)))){
						populateList(resultList,StringUtils.trim(link.attr(Constants.TAG_LINK_ATTR_HREF)),StringUtils.trim(link.text()),parentCrawlURI);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return resultList;
	}
	/**
     * 取得指定区域的HTML内容
     * @param orginHtml 原始HTML
     * @param fetchAreaTagMap 待提取区域标签属性Map
	 * @param deleteAreaTagMap 待提取区域中要删除的标签属性Map
	 * @param contentHandleMap 内容处理Map
     * @return 指定区域的HTML内容
     */
    @Override
	public String getHtml(String orginHtml,Map<String,String> fetchAreaTagMap,Map<String,String> deleteAreaTagMap,Map<String,String> contentHandleMap) {
		if (StringUtils.isNotBlank(orginHtml)) {
			orginHtml = getHtmlByFilter(fetchAreaTagMap, deleteAreaTagMap,contentHandleMap,orginHtml);
		}
		return orginHtml;
	}
	/**
     * 取得指定区域中指定属性标签内的HTML内容
     * @param fetchAreaTagMap 待提取区域标签属性Map
     * @return 指定区域的HTML内容
     */
    @Override
	public String fetchAreaHtml(String orginHtml,Map<String,String> fetchAreaTagMap){
		log.info("========取得指定区域的HTML内容=========");
		if (StringUtils.isNotBlank(orginHtml)) {
			try {
				orginHtml = filterRequire(fetchAreaTagMap,orginHtml);
				log.info(orginHtml);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return orginHtml;
	}
	/**
     * 取得指定区域中指定属性标签内的HTML内容
     * @param fetchAreaTagMap 待提取区域标签属性Map
     * @return 指定区域的HTML内容
     */
    @Override
	public List<String> fetchAreaHtmlForList(String orginHtml,Map<String,String> fetchAreaTagMap){
		log.info("========取得指定区域的HTML内容=========");
		List<String> returnHtmlList = new ArrayList<>();
		if (StringUtils.isNotBlank(orginHtml)) {
			try {
				returnHtmlList = filterRequireForList(fetchAreaTagMap,orginHtml);
				log.info(orginHtml);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return returnHtmlList;
	}
	/**
     * 过滤指定区域的HTML内容
	 * @param deleteAreaTagMap 待提取区域中要删除的标签属性Map
     * @return 过滤后的HTML内容
     */
    @Override
	public String deleteAreaHtml(String orginHtml,Map<String,String> deleteAreaTagMap){
		log.info("========过滤指定区域的HTML内容=========");
		return filterHtml(deleteAreaTagMap,orginHtml);
	}
	/**
	 * 取得无格式的HTML内容
	 * @param orginHtml 原始HTML内容
	 * @return 无格式的HTML内容
	 */
    @Override
	public String getPlainText(String orginHtml){
		log.info("========取得无格式的HTML内容=========");
		String resultStr = "";
		try {
			Document doc = Jsoup.parse(orginHtml);
			resultStr = doc.body().text();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultStr.replaceAll("\\s", "");
	}
	/**
	 * 取得HTML内容中的资源
	 * @param orginHtml 原始HTML内容
	 * @param resCrawlURIList 资源URI对象集合
	 * @param parentCrawlURI 父crawlURI
	 * @return String
	 */
    @Override
	public String getHtmlResource(String orginHtml,List<CrawlResURI> resCrawlURIList,CrawlURI parentCrawlURI){
		log.info("========取得HTML内容中的资源=========");
		if (StringUtils.isNotBlank(orginHtml)) {
			try {
				Document doc = Jsoup.parse(orginHtml);
				orginHtml = extractImageTag(doc,orginHtml,resCrawlURIList,parentCrawlURI);
				extractLinkRes(doc, resCrawlURIList,parentCrawlURI);
				orginHtml = extractObject(doc,orginHtml, resCrawlURIList,parentCrawlURI);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return orginHtml;
		
	}
	/**
	 * 取得指定区域内的内容并放如集合中
	 * @param orginHtml 原始HTML内容
     * @param fetchAreaTagMap 待提取区域标签属性Map
	 * @return 指定区域内的内容集合
	 */
    @Override
	public List<String> getHtmlList(String orginHtml,Map<String,String> fetchAreaTagMap){
		List<String> htmlList = new ArrayList<String>();
		log.info("========取得指定区域的HTML内容=========");
		if (StringUtils.isNotBlank(orginHtml)) {
			try {
				Document doc = Jsoup.parse(orginHtml);
				// 第一步取得指定属性/标签内容
				List<Map<String,String[]>> nodeFilterList = populateFilter(fetchAreaTagMap);
				for(Map<String,String[]> filter : nodeFilterList){
					addHtmlToListByFilter(doc,filter, htmlList);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return htmlList;
	}
	
	private String filterHtml(Map<String,String> filterMap,String orginHtml){
		log.info("========过滤指定区域的HTML内容=========");
		if (StringUtils.isNotBlank(orginHtml)) {
			try {
				orginHtml = filterThrowAway(filterMap,orginHtml);
				orginHtml = filterOtherTag(orginHtml);
				log.info(orginHtml);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return orginHtml;
	}
	
	/**
	 * 提取内容中图片标签内容
	 * @param doc html文档对象
	 * @param orginHtml 原始HTML
	 * @param resCrawlURIList 资源URI对象集合
	 * @param parentCrawlURI 父crawlURI
	 * @throws ParserException 
	 */
	private String extractImageTag(Document doc,String orginHtml, List<CrawlResURI> resCrawlURIList,CrawlURI parentCrawlURI){
		log.info("========提取内容中图片标签内容=========");
		List<CrawlResURI> resultCrawlResURIList = new CopyOnWriteArrayList<CrawlResURI>();
		Elements imgs = doc.getElementsByTag(Constants.TAG_IMG);
		String orginImgUrl = null;
		String orginImgTagHtml = null;
		String newImgTagHtml = null;
		String tag = Constants.TAG_IMG_SRC;
		for(Element img : imgs){
			orginImgUrl = img.attr(tag);
			if(StringUtils.isBlank(orginImgUrl)){
				tag = Constants.TAG_IMG_DATA_SRC;
				orginImgUrl = img.attr(tag);
			}
			if(StringUtils.isNotBlank(orginImgUrl)){
				if(!orginImgUrl.contains("http://")){
					orginImgTagHtml = img.html();
					CrawlResURI crawlResURI = uriHelper.populateResURI(parentCrawlURI,orginImgUrl, "",Constants.RES_IMAGES_MAP_KEY);
					resultCrawlResURIList.add(crawlResURI);
					orginImgUrl = crawlResURI.getUrl().getUrl();
					//设置新URL
					img.attr(tag,orginImgUrl);
					newImgTagHtml =  img.html();
					orginHtml = StringUtils.replace(orginHtml, orginImgTagHtml, newImgTagHtml);
				}else{
					resultCrawlResURIList.add(uriHelper.populateResURI(parentCrawlURI,orginImgUrl, "",Constants.RES_IMAGES_MAP_KEY));
				}
			}
		}
		resCrawlURIList.addAll(resultCrawlResURIList);
		return orginHtml;
	}
	/**
	 * 提取内容连接中的资源
	 * @param doc html文档对象
	 * @param resCrawlURIList 资源URI对象集合
	 * @param parentCrawlURI 父crawlURI
	 * @throws ParserException 
	 */
	private void extractLinkRes(Document doc, List<CrawlResURI> resCrawlURIList,CrawlURI parentCrawlURI){
		log.info("========提取内容连接中的资源=========");
		List<CrawlResURI> resultCrawlResURIList = new CopyOnWriteArrayList<CrawlResURI>();
		Elements links = doc.getElementsByTag(Constants.TAG_LINK);
		for(Element link : links){
			if(StringUtils.isNotBlank(link.attr(Constants.TAG_LINK_ATTR_HREF)) && 
					!Constants.EMPTY_LINK_STR.equals(StringUtils.trim(link.attr(Constants.TAG_LINK_ATTR_HREF)))){
				resultCrawlResURIList.add(uriHelper.populateResURI(parentCrawlURI,link.attr(Constants.TAG_LINK_ATTR_HREF), "",Constants.RES_ATTAC_MAP_KEY));
			}
		}
		resCrawlURIList.addAll(resultCrawlResURIList);
	}
	/**
	 * 提取内容中对象资源，如，媒体资源
	 * @param doc html文档对象
	 * @param resCrawlURIList 资源URI对象集合
	 * @param parentCrawlURI 父crawlURI
	 */
	private String extractObject(Document doc,String orginHtml,List<CrawlResURI> resCrawlURIList,CrawlURI parentCrawlURI){
		log.info("========提取内容中对象资源=========");
		List<CrawlResURI> resultCrawlResURIList = new CopyOnWriteArrayList<CrawlResURI>();
		Elements objs = doc.getElementsByTag(Constants.TAG_EMBED);
		String url = "";
		String urlTypeName = "";
		for(Element obj : objs){
			url = obj.attr(Constants.TAG_EMBED_SRC);
			urlTypeName = FilenameUtils.getExtension(url.toLowerCase());
			if(StringUtils.isNotBlank(url) && StringUtils.isNotBlank(urlTypeName) && Constants.EXTRACT_MEDIA_RES_TYPE.contains(urlTypeName)){
				resultCrawlResURIList.add(uriHelper.populateResURI(parentCrawlURI,url, "",Constants.RES_MEDIA_MAP_KEY));
				//去掉原始OBJ对象
				orginHtml = StringUtils.replace(orginHtml, obj.html(), "");
			}
		}
		resCrawlURIList.addAll(resultCrawlResURIList);
		return orginHtml;
	}
	/**
	 * 替换HTML内容中资源链接地址为指定地址，并返回包含了替换后HTML，原始资源链接与替换后资源链接对照的MAP对象
	 * @param orginHtml 原始HTML内容
	 * @param task 任务对象
	 */
	public String replaceHtmlResource(String orginHtml,Task task){
		if(StringUtils.isNotBlank(orginHtml)){
			try {
				Document doc = Jsoup.parse(orginHtml);
				orginHtml = replaceImages(orginHtml, task, doc);
				orginHtml = replaceLinkRes(orginHtml, task, doc);
				orginHtml = replaceObject(orginHtml,task, doc);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return orginHtml;
	}
	/**
	 * 替换图片
	 * @param orginHtml 原始HTML
	 * @param task 任务对象
	 * @param doc html文档对象
	 * @return
	 */
	private String replaceImages(String orginHtml,Task task, Document doc){
		log.info("========替换图片=========");
		//图片地址MAP：KEY为新图片地址，value为源图片地址
		List<CrawlResURI> resultCrawlResURIList = new CopyOnWriteArrayList<CrawlResURI>();
		Elements imgs = doc.getElementsByTag(Constants.TAG_IMG);
		//原始图片标签HTML
		String orginImgTagHtml = null;
		//原始图片标签URL
		String orginImgTagUrl = null;
		//新图片标签HTML
		String newImgTagHtml = null;
		//新图片标签URL
		String newImgTagUrl = null;
		String tag = Constants.TAG_IMG_SRC;
		for(Element img : imgs){
			orginImgTagUrl = img.attr(tag);
			if(StringUtils.isBlank(orginImgTagUrl)){
				tag = Constants.TAG_IMG_DATA_SRC;
				orginImgTagUrl = img.attr(tag);
			}
			if(StringUtils.isBlank(orginImgTagUrl)){
				continue;
			}
			orginImgTagHtml = img.outerHtml();
			newImgTagUrl = populateSavePath(orginImgTagUrl,task,"");
			//设置新URL
			img.attr(tag, newImgTagUrl);
			newImgTagHtml =  img.outerHtml();
			resultCrawlResURIList.add(uriHelper.populateResURI(task.getCrawlURI(),orginImgTagUrl, newImgTagUrl,Constants.RES_IMAGES_MAP_KEY));
			orginHtml = StringUtils.replace(orginHtml, orginImgTagHtml, newImgTagHtml);
		}
		if(!resultCrawlResURIList.isEmpty()){
			task.getContentBean().getResCrawlURIList().addAll(resultCrawlResURIList);
		}
		return orginHtml;
	}
	/**
	 * 替换内容中连接资源
	 * @param orginHtml 原始HTML
	 * @param task 任务对象
	 * @param doc HTML文档对象
	 * @return
	 */
	private String replaceLinkRes(String orginHtml,Task task, Document doc){
		log.info("========替换内容中连接资源=========");
		//资源地址MAP：KEY为新资源地址，value为源资源地址
		List<CrawlResURI> resultCrawlResURIList = new CopyOnWriteArrayList<CrawlResURI>();
		Elements links = doc.getElementsByTag(Constants.TAG_LINK);
		String urlTypeName = "";
		//原始资源标签HTML
		String orginTagHtml = null;
		//原始资源标签URL
		String orginTagUrl = null;
		//新资源标签HTML
		String newTagHtml = null;
		//新资源标签URL
		String newTagUrl = null;
		
		for(Element link : links){
			orginTagUrl = link.attr(Constants.TAG_LINK_ATTR_HREF);
			urlTypeName = FilenameUtils.getExtension(orginTagUrl.toLowerCase());
			if(StringUtils.isNotBlank(orginTagUrl) && StringUtils.isNotBlank(urlTypeName) && Constants.EXTRACT_MEDIA_RES_TYPE.contains(urlTypeName)){
				orginTagHtml = link.outerHtml();
				newTagUrl = populateSavePath(orginTagUrl,task,Constants.EXTRACT_RES_TYPE);
				//设置新URL
				link.attr(Constants.TAG_LINK_ATTR_HREF,newTagUrl);
				newTagHtml =  link.outerHtml();
				if(StringUtils.isNotBlank(newTagUrl)){
					resultCrawlResURIList.add(uriHelper.populateResURI(task.getCrawlURI(),orginTagUrl, newTagUrl,Constants.RES_ATTAC_MAP_KEY));
				}
				orginHtml = StringUtils.replace(orginHtml, orginTagHtml, newTagHtml);
			}
		}
		task.getContentBean().getResCrawlURIList().addAll(resultCrawlResURIList);
		return orginHtml;
	}
	/**
	 * 替换内容中对象资源
	 * @param orginHtml 原始HTML
	 * @param task 任务对象
	 * @param doc html文档对象
	 * @return
	 */
	private String replaceObject(String orginHtml,Task task, Document doc){
		log.info("========替换内容中对象资源=========");
		//资源地址MAP：KEY为新资源地址，value为源资源地址
		List<CrawlResURI> resultCrawlResURIList = new CopyOnWriteArrayList<CrawlResURI>();
		Elements objs = doc.getElementsByTag(Constants.TAG_EMBED);
		//原始资源标签HTML
		String orginTagHtml = null;
		//原始资源标签URL
		String orginTagUrl = null;
		//新资源标签HTML
		String newTagHtml = null;
		//新资源标签URL
		String newTagUrl = null;
		String urlTypeName = "";
		for(Element obj : objs){
			orginTagUrl = obj.attr(Constants.TAG_EMBED_SRC);
			urlTypeName = FilenameUtils.getExtension(orginTagUrl.toLowerCase());
			if(StringUtils.isNotBlank(orginTagUrl) && StringUtils.isNotBlank(urlTypeName) && Constants.EXTRACT_MEDIA_RES_TYPE.contains(urlTypeName)){
				orginTagHtml = obj.outerHtml();
				//newTagUrl = populateSavePath(orginTagUrl,savePath,Constants.EXTRACT_MEDIA_RES_TYPE);
				newTagUrl = orginTagUrl;//暂时不替换视频对象
				//设置新URL
				obj.attr(Constants.TAG_EMBED_SRC,newTagUrl);
				newTagHtml =  obj.outerHtml();
				if(StringUtils.isNotBlank(newTagUrl)){
					resultCrawlResURIList.add(uriHelper.populateResURI(task.getCrawlURI(),orginTagUrl, newTagUrl,Constants.RES_MEDIA_MAP_KEY));
				}
				//去掉原来OBJ对象
				orginHtml = StringUtils.replace(orginHtml, orginTagHtml, "");
				//orginHtml = StringUtils.replace(orginHtml, orginTagHtml, newTagHtml);
			}
		}
		task.getContentBean().getResCrawlURIList().addAll(resultCrawlResURIList);
		return orginHtml;
	}
	/**
	 * 组装新地址
	 * @param originResTagUrl 原始资源地址
	 * @param task 任务对象
	 * @param allowResTypeStr 允许采集回来的资源类型str
	 * @return
	 */
	private String populateSavePath(String originResTagUrl,Task task,String allowResTypeStr){
		StringBuilder resultStr = new StringBuilder();
		String newRriginResTagUrl = originResTagUrl;
		//处理资源路径带?号的情况
		if(newRriginResTagUrl.contains("?")){
			newRriginResTagUrl = newRriginResTagUrl.substring(0,newRriginResTagUrl.indexOf("?"));
		}
		String savePath = task.getController().getCrawlScope().getSavePath();
		String resTypeName = FilenameUtils.getExtension(originResTagUrl);
		boolean extIsEmpty = false;
		//如果扩展名为空
		if(StringUtils.isBlank(resTypeName)){
			extIsEmpty = true;
			resTypeName = getExtension(task,originResTagUrl);
		}
		if(StringUtils.isNotBlank(newRriginResTagUrl) && StringUtils.isNotBlank(savePath) && StringUtils.isNotBlank(resTypeName) && (StringUtils.isBlank(allowResTypeStr) || allowResTypeStr.contains(resTypeName.toLowerCase()))){
			if(Boolean.valueOf(Constants.REPLACE_NAME) || extIsEmpty){
				resultStr.append(savePath).append(generator.next()).append(Constants.IMAGE_SPLIT).append(resTypeName);
			}else{
				resultStr.append(savePath).append(FilenameUtils.getName(newRriginResTagUrl));
			}
		}
		return resultStr.toString();
	}
	/**
	 * 获取扩展名
	 * <p>说明:</p>
	 * <li></li>
	 * @param task
	 * @param originResTagUrl
	 * @return
	 * @since 2017年9月14日下午1:41:21
	 */
	private String getExtension(Task task, String originResTagUrl) {
		try {
			CrawlResURI crawlURI = uriHelper.populateResURI(task.getCrawlURI(),originResTagUrl, "",Constants.RES_IMAGES_MAP_KEY);
			if(null != crawlURI){
				HttpResponse rsp = task.getController().getHttpHandler().getSync(HttpUrlHelper.getRawUrl(crawlURI),HttpUrlHelper.getHeaderInfo());
				if (rsp != null) {
					log.info("=========获取扩展名========="+rsp.getContentType());
					return Constants.CONTENT_TYPE.get(rsp.getContentType());
				}
			}
		}  catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 替换内容中的超链接
	 * @param orginHtml 原始HTML内容
	 * @return 去掉超链接后的HTML
	 */
	public String replaceHtmlLink(String orginHtml){
		log.info("========替换内容中的超链接=========");
		if(StringUtils.isNotBlank(orginHtml)){
			try {
				Document doc = Jsoup.parse(orginHtml);
				Elements links = doc.getElementsByTag(Constants.TAG_LINK);
				String tempStr = "";
				for(Element link : links){
					tempStr = link.text();
//					if(link.childNodeSize() > 0){
//						tempStr = link.html();
//					}
					orginHtml = StringUtils.replace(orginHtml, link.html(), tempStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return orginHtml;
	}
	/**
	 * 取得连接标题Map，如果有标题图片则放到资源MAP中
	 * @param task 任务对象
     * @param fetchAreaTagMap 待提取区域标签属性Map
     * @param deleteAreaTagMap 待提取区域中要删除的标签属性Map
	 * @param contentHandleMap 内容处理Map
	 * @return 连接集合
	 */
    @Override
	public List<CrawlLinkURI> getCrawlURIList(Task task,Map<String,String> fetchAreaTagMap,Map<String,String> deleteAreaTagMap,Map<String,String> contentHandleMap){
		return getUrlAndTitleAndImagesMap(task,fetchAreaTagMap,deleteAreaTagMap,contentHandleMap);
	}
	
	/**
	 * 得到连接标题MAP
	 * @param task 任务对象
     * @param fetchAreaTagMap 待提取区域标签属性Map
     * @param deleteAreaTagMap 待提取区域中要删除的标签属性Map
	 * @param contentHandleMap 内容处理Map
	 * @return 连接或者标题集合
	 */
	private List<CrawlLinkURI> getUrlAndTitleAndImagesMap(Task task,Map<String,String> fetchAreaTagMap,Map<String,String> deleteAreaTagMap,Map<String,String> contentHandleMap) {
		log.info("========开始得到连接标题MAP=========savePath:"+task.getController().getCrawlScope().getSavePath());
		log.info("========连接标题MAP=========getLinksetStartMap："+fetchAreaTagMap+",getLinksetEndMap:"+deleteAreaTagMap);
		List<CrawlLinkURI> crawlList = new CopyOnWriteArrayList<CrawlLinkURI>();
		try {
			String html = task.getContentBean().getOrginHtml();
			html = getHtmlByFilter(fetchAreaTagMap, deleteAreaTagMap,contentHandleMap,html);
			Document doc = Jsoup.parse(html);
			Elements links = doc.getElementsByTag(Constants.TAG_LINK);
			
			CrawlLinkURI crawlURI = null;
			String newImagePath = "";
			String orignImagePath = "";
			String title = "";
			String url = "";
			for (Element link : links) {
				//orignImagePath = "";
				//newImagePath = "";
				title = link.text();
				url = link.attr(Constants.TAG_LINK_ATTR_HREF);
				//取得连接中的子标签
				if(link.childNodeSize() > 0){
					for(Node child : link.childNodes()){
						//如果有图片
						if(Constants.TAG_IMG.equals(child.nodeName()) && (child.hasAttr(Constants.TAG_IMG_SRC) || child.hasAttr(Constants.TAG_IMG_DATA_SRC))){
							orignImagePath = child.attr(Constants.TAG_IMG_SRC);
							if(StringUtils.isBlank(orignImagePath)){
								orignImagePath = child.attr(Constants.TAG_IMG_DATA_SRC);
							}
							if(StringUtils.isBlank(orignImagePath)){
								continue;
							}
							newImagePath = populateSavePath(orignImagePath,task,"");
							if(StringUtils.isNotBlank(child.attr(Constants.TAG_ATTR_TITLE))){
								title = child.attr(Constants.TAG_ATTR_TITLE);
							}
							break;
						}
					}
				}
				//如果标题还是为空，则以url的hashCode为标题
				if(StringUtils.isBlank(title) && StringUtils.isNotBlank(url)){
					title = String.valueOf(url.hashCode());
				}
				if(StringUtils.isNotBlank(url)){
					crawlURI = uriHelper.populateCrawlURI(task.getCrawlURI(),url,title);
					if(StringUtils.isNotBlank(orignImagePath)){
						CrawlResURI resURI = uriHelper.populateResURI(task.getCrawlURI(),orignImagePath, newImagePath,Constants.RES_IMAGES_MAP_KEY);
						crawlURI.setResURI(resURI);
					}
					crawlList.add(crawlURI);
				}
			}
			return crawlList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 组装CrawlLinkURI集合
	 * @param list 连接集合
	 * @param urlStr 连接
	 * @param title 标题
	 * @param parentCrawlURI 父CrawlURI对象
	 */
	private void populateList(List<CrawlLinkURI> list,String urlStr,String title,CrawlURI parentCrawlURI){
		urlStr = StringUtils.trim(urlStr);
		if(StringUtils.isNotBlank(urlStr)){
			CrawlLinkURI url = uriHelper.populateCrawlURI(parentCrawlURI,urlStr,title);
			if(!list.contains(url)){
				list.add(url);
			}
		}
	}
	/**
     * 取得指定区域的HTML内容
  	 * @param fetchAreaTagMap 待提取区域标签属性Map
	 * @param deleteAreaTagMap 待提取区域中要删除的标签属性Map
	 * @param contentHandleMap 内容处理Map
     * @param orginHtml 原始HTML
     * @return 指定区域的HTML内容
     * @throws ParserException
     */
	private String getHtmlByFilter(Map<String, String> fetchAreaTagMap,
			Map<String, String> deleteAreaTagMap,Map<String,String> contentHandleMap, String orginHtml) {
		if(StringUtils.isNotBlank(orginHtml)){
			try {
				// 第一步取得指定属性/标签内容
				orginHtml = filterRequire(fetchAreaTagMap,orginHtml);
				// 第二步过滤指定属性/标签内容
				orginHtml = filterThrowAway(deleteAreaTagMap, orginHtml);
				// 第三步过滤其他标签
				orginHtml = filterOtherTag(orginHtml);
				log.info("=================================采集结果=======================================");
				log.info(orginHtml);
				// 第四步处理内容
//				orginHtml = contentHandle(orginHtml,contentHandleMap);
//				log.info("=================================处理后结果=======================================");
//				log.info(orginHtml);
				return orginHtml;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "";
	}
	
	/**
	 * 采集指定属性/标签内容
	 * 如果没有找到，则在注释中查找
	 * @param tagMap 指定属性/标签
	 * @param orginHtml 原始HTML
	 * @return
	 */
	private String filterRequire(Map<String, String> tagMap,String orginHtml){
		String resultHtml = getNaturalHtml(tagMap,orginHtml);
		//如果没有找到，则在注释内容中找
		if(StringUtils.isEmpty(resultHtml)){
			List<String> remarkList = new ArrayList<String>();
			findAllRemark(remarkList,orginHtml);
			for(String remark : remarkList){
				resultHtml = getNaturalHtml(tagMap,remark);
				if(StringUtils.isNotBlank(resultHtml)) break;
			}
		}
		Document tempDoc = Jsoup.parse(resultHtml);
		return tempDoc.body().html();
	}
	/**
	 * 采集指定属性/标签内容
	 * 如果没有找到，则在注释中查找
	 * @param tagMap 指定属性/标签
	 * @param orginHtml 原始HTML
	 * @return
	 */
	private List<String> filterRequireForList(Map<String, String> tagMap,String orginHtml){
		List<String> tempHtmlList = getNaturalHtmlForList(tagMap,orginHtml);
		//如果没有找到，则在注释内容中找
		if(CollectionUtils.isEmpty(tempHtmlList)){
			List<String> remarkList = new ArrayList<String>();
			findAllRemark(remarkList,orginHtml);
			for(String remark : remarkList){
				tempHtmlList = getNaturalHtmlForList(tagMap,remark);
				if(CollectionUtils.isEmpty(tempHtmlList)) break;
			}
		}
		List<String> resultHtmlList = new ArrayList<>();
		for(String html : tempHtmlList){
			Document tempDoc = Jsoup.parse(html);
			resultHtmlList.add(tempDoc.body().html());
		}
		return resultHtmlList;
	}
	/**
	 * 递归取得所有注释内容
	 * @param remarkList
	 * @param orginHtml
	 */
	private void findAllRemark(List<String> remarkList, String orginHtml){
		String resultHtml = getRemarkHtml(orginHtml);
		if(orginHtml.equals(resultHtml)){
			return;
		}
		if(StringUtils.isNotBlank(resultHtml)){
			remarkList.add(resultHtml);
			findAllRemark(remarkList,resultHtml);
		}
	}
	/**
	 * 在正常的HTML内容中采集指定属性/标签内容
	 * @param tagMap 指定属性/标签
	 * @param orginHtml 原始HTML
	 * @return
	 * @throws ParserException
	 */
	private String getNaturalHtml(Map<String, String> tagMap, String orginHtml){
		if(null != tagMap && !tagMap.isEmpty()){
			log.info("========原始HTML=========");
			log.info(orginHtml);
			log.info("========开始采集指定属性/标签内容=========");
			log.info("指定属性/标签========="+tagMap);
			Document doc = Jsoup.parse(orginHtml);
			StringBuilder sb = new StringBuilder();
			// 第一步取得指定属性/标签内容
			List<Map<String,String[]>> nodeFilterList = populateFilter(tagMap);
			for(Map<String,String[]> filter : nodeFilterList){
				appendHtmlByFilter(doc, filter, sb);
			}
			log.info("========采集指定属性/标签内容结果=========");
			log.info(sb.toString());
			return sb.toString();
		}
		return orginHtml;
	}
	/**
	 * 在正常的HTML内容中采集指定属性/标签内容
	 * @param tagMap 指定属性/标签
	 * @param orginHtml 原始HTML
	 * @return
	 * @throws ParserException
	 */
	private List<String> getNaturalHtmlForList(Map<String, String> tagMap, String orginHtml){
		List<String> returnHtmlList = new ArrayList<>();
		if(null != tagMap && !tagMap.isEmpty()){
			log.info("========原始HTML=========");
			log.info(orginHtml);
			log.info("========开始采集指定属性/标签内容=========");
			log.info("指定属性/标签========="+tagMap);
			Document doc = Jsoup.parse(orginHtml);
			// 第一步取得指定属性/标签内容
			List<Map<String,String[]>> nodeFilterList = populateFilter(tagMap);
			for(Map<String,String[]> filter : nodeFilterList){
				appendHtmlByFilterForList(doc, filter, returnHtmlList);
			}
			log.info("========采集指定属性/标签内容结果=========");
			log.info(returnHtmlList.toString());
			return returnHtmlList;
		}
		returnHtmlList.add(orginHtml);
		return returnHtmlList;
	}
	/**
	 * 取得注释内容：用正则表达式匹配
	 * @param orginHtml 原始HTML
	 * @return 注释内容
	 */
	private String getRemarkHtml(String orginHtml){
		log.info("========取得页面注释内容=========");
		try {
			StringBuilder sb = new StringBuilder();
			Document doc = Jsoup.parse(orginHtml);
			Node child = null;
			for (int i = 0; i < doc.childNodes().size();i++) {
				child = doc.childNode(i);
				if(Constants.TAG_COMMENT.equals(child.nodeName())){
					log.info("========页面注释标签========="+child.nodeName());
					sb.append(child.toString());
				}
			}
//			Pattern p = Pattern.compile(Constants.REMARK_REGEX);
//			Matcher m=p.matcher(orginHtml);
//			while(m.find()){
//			   System.out.println(m.group());
//			}
	        return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	/**
	 * 取得指定属性/标签无格式内容集合
	 * @param orginHtml 原始HTML
	 * @param fetchAreaTagMap 指定属性/标签
	 * @return 无格式内容集合
	 * @throws ParserException
	 */
    @Override
	public List<String> getPlainTextList(String orginHtml,Map<String, String> fetchAreaTagMap,Map<String,String> contentHandleMap){
		List<String> list = new CopyOnWriteArrayList<String>();
		if(StringUtils.isNotBlank(orginHtml)){
			try {
				Document doc = Jsoup.parse(orginHtml);
				// 取得指定属性/标签内容
				List<Map<String,String[]>> nodeFilterList = populateFilter(fetchAreaTagMap);
				for(Map<String,String[]> filter : nodeFilterList){
					addPlainTextToList(doc, filter, list,contentHandleMap);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	/**
	 * 过滤其他标签
	 * @param contentHtml 原始内容
	 * @return 过滤后内容
	 * @throws ParserException
	 */
	private String filterOtherTag(String contentHtml){
		log.info("========开始过滤其他标签=========");
		Document doc = Jsoup.parse(contentHtml);
		//过滤注释
		Map<String,String[]> filter = new HashMap<String,String[]>();
		filter.put(Constants.SINGLE_TAG, new String[]{Constants.TAG_COMMENT});
		contentHtml = removeHtmlByFilter(doc, filter, contentHtml);
		return contentHtml;
	}
	/**
	 * 过滤指定标签
	 * @param removeTagMap 指定标签属性MAP
	 * @param orginHtml
	 * @return
	 * @throws ParserException
	 */
	private String filterThrowAway(Map<String, String> removeTagMap, String orginHtml) {
		log.info("========开始过滤指定标签=========");
		if(null != removeTagMap && !removeTagMap.isEmpty()){
			List<Map<String,String[]>> nodeFilterList = populateFilter(removeTagMap);
			Document doc = Jsoup.parse(orginHtml);
			for(Map<String,String[]> filter : nodeFilterList){
				orginHtml = removeHtmlByFilter(doc,filter, orginHtml);
			}
		}
		return orginHtml;
	}
	

	/**
     * 过滤指定属性标签HTML
	 * @param doc html文档
     * @param filter 属性过滤器
     * @param orginHtml 原始HTML
     * @return 过滤后HTML
     * @throws ParserException
     */
	private String removeHtmlByFilter(Document doc, Map<String,String[]> filter,String orginHtml) {
		String[] values = null;
		Elements tags = null;
		Document tempDoc = null;
		String deleteHtml = "";
		for(String key : filter.keySet()){
			//处理单标签
			if(Constants.SINGLE_TAG.equals(key)){
				values = filter.get(key);
//				tempDoc = parserTags(doc,values);
//				deleteHtml = tempDoc.body().html();
//				orginHtml = doc.outerHtml();
//				orginHtml = orginHtml.replace(deleteHtml, "");
				for(String v : values){
					tags = doc.select(v);
					if(null != tags){
						tags.remove();
						orginHtml = doc.outerHtml();
					}
				}
			}else{//否则查找属性
				values = filter.get(key);
				for(String v : values){
					tags = doc.getElementsByAttributeValueMatching(key, v);
//					for(Element e : tags){
//						orginHtml = StringUtils.remove(orginHtml, e.outerHtml());
//					}
					if(null != tags){
						tags.remove();
						orginHtml = doc.outerHtml();
					}
				}
			}
		}
		Document resultDoc = Jsoup.parse(orginHtml);
		return resultDoc.body().html();
	}

	/**
	 * 取得所有指定属性/标签的HTML
	 * @param doc html文档
	 * @param filter 过滤器
	 * @param sb 
	 * @throws ParserException
	 */
	private void appendHtmlByFilter(Document doc, Map<String,String[]> filter,
			StringBuilder sb) {
		log.info("========开始取得所有指定属性/标签的HTML========="+filter.toString());
		String[] values = null;
		Elements tags = null;
		Document tempDoc = null;
		for(String key : filter.keySet()){
			//处理单标签
			if(Constants.SINGLE_TAG.equals(key)){
				values = filter.get(key);
				tempDoc = parserTags(doc,values);
				sb.append(tempDoc.outerHtml());
			}else{//否则查找属性
				values = filter.get(key);
				for(String v : values){
					tags = doc.getElementsByAttributeValueMatching(key, v);
					for(Element e : tags){
						sb.append(e.outerHtml());
						log.info("=========================HTML：========="+e.outerHtml());
					}
				}
			}
		}
	}
	/**
	 * 取得所有指定属性/标签的HTML
	 * @param doc html文档
	 * @param filter 过滤器
	 * @param htmlList 
	 * @throws ParserException
	 */
	private void appendHtmlByFilterForList(Document doc, Map<String,String[]> filter,
			List<String> htmlList) {
		log.info("========开始取得所有指定属性/标签的HTML========="+filter.toString());
		String[] values = null;
		Elements tags = null;
		for(String key : filter.keySet()){
			//处理单标签
			if(Constants.SINGLE_TAG.equals(key)){
				values = filter.get(key);
				htmlList.addAll(parserTagsForList(doc,values));
			}else{//否则查找属性
				values = filter.get(key);
				for(String v : values){
					tags = doc.getElementsByAttributeValueMatching(key, v);
					for(Element e : tags){
						htmlList.add(e.outerHtml());
						log.info("=========================HTML：========="+e.outerHtml());
					}
				}
			}
		}
	}
	
	/**
	 * 解析标签
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-12-2下午3:07:38
	 * @version 1.0
	 * @param doc
	 * @param tagArr
	 * @return
	 */
	protected Document parserTags(Document doc,String[] tagArr){
		Elements tags = null;
		Elements tempTags = null;
		Element tag = null;
		String pv = "";
		StringBuilder tempHtml = new StringBuilder();
		int pos = 0;
		Document tempDoc = Jsoup.parse(doc.outerHtml());
		for(String v : tagArr){
			tempDoc = Jsoup.parse(doc.outerHtml());
			List<Map<String,String>> params = parserParams(v);
			for(Map<String,String> p : params){
				for(String pk : p.keySet()){
					pv = p.get(pk);
					if(StringUtils.isNotBlank(pv) && StringUtils.isNumeric(pv)){
						tags = tempDoc.select(pk);
						if(tags.size() > pos){
							tag = tags.get(Integer.valueOf(pv));
							tempDoc = Jsoup.parse(tag.outerHtml());
						}else{
							tempDoc = Jsoup.parse(tags.outerHtml());
						}
					}else{
						tags = tempDoc.select(pk);
						tempDoc = Jsoup.parse(tags.outerHtml());
					}
					tempDoc = parseJs(tempDoc);
				}
			}
			tempTags = tempDoc.body().children();
			if(tempTags.size() > 1){
				tempHtml = new StringBuilder();
				for(int i = 0;i < tempTags.size() ; i++){
					tempHtml.append(tempTags.get(i).outerHtml());
					if(i < tempTags.size() - 1){
						tempHtml.append(",");
					}
				}
				tempDoc = Jsoup.parse(tempHtml.toString());
			}
		}
		return tempDoc;
	}
	protected List<String> parserTagsForList(Document doc,String[] tagArr){
		Elements tags = null;
		Elements tempTags = null;
		Element tag = null;
		String pv = "";
		List<String> tempHtml = new ArrayList<>();
		int pos = 0;
		Document tempDoc = Jsoup.parse(doc.outerHtml());
		for(String v : tagArr){
			tempDoc = Jsoup.parse(doc.outerHtml());
			List<Map<String,String>> params = parserParams(v);
			for(Map<String,String> p : params){
				for(String pk : p.keySet()){
					pv = p.get(pk);
					if(StringUtils.isNotBlank(pv) && StringUtils.isNumeric(pv)){
						tags = tempDoc.select(pk);
						if(tags.size() > pos){
							tag = tags.get(Integer.valueOf(pv));
							tempDoc = Jsoup.parse(tag.outerHtml());
						}else{
							tempDoc = Jsoup.parse(tags.outerHtml());
						}
					}else{
						tags = tempDoc.select(pk);
						tempDoc = Jsoup.parse(tags.outerHtml());
					}
					tempDoc = parseJs(tempDoc);
				}
			}
			tempTags = tempDoc.body().children();
			if(tempTags.size() > 1){
				String temp = "";
				for(int i = 0;i < tempTags.size() ; i++){
					temp = tempTags.get(i).outerHtml();
					tempDoc = Jsoup.parse(temp);
					tempHtml.add(tempDoc.outerHtml());
				}
			}
		}
		return tempHtml;
	}
    /**
     * 执行JS
     * <p>说明:</p>
     * <li></li>
     * @auther DuanYong
     * @since 2016年6月16日下午5:02:26
     * @param doc
     * @return 返回JS生成的内容
     */
	private Document parseJs(Document doc) {
		Elements els = doc.select(Constants.TAG_JS);
		Document tempDoc = null;
		for(Element el: els) {
		   //提取src信息
		   String url = el.attr(Constants.TAG_JS_ATTR_SRC);
		   if(StringUtils.isBlank(url)){
			   continue;
		   }
			try {
				URL u = getURL(url);
				tempDoc = Jsoup.parse(u, 2000);
		        String result = executeJs(tempDoc.body().text());
		        result = doc.outerHtml().replace(el.outerHtml(), result);
		        doc = Jsoup.parse(result);
			} catch(Exception e) {
				log.error(e.getMessage(), e);
				//e.printStackTrace();
				continue;
			}
		}
		return doc;
	}
	
	private URL getURL(String url) {
		Task task = SwapAreaUtils.getTask();
		try {
			if(null == task || url.startsWith(Constants.HTTP_FILL_KEY)){
				return new URL(url);
			}
			if(!url.startsWith("/")){
				url = "/" + url;
			}
			if(task.getCrawlURI().getPort() != -1){
				url = Constants.HTTP_FILL_KEY+task.getCrawlURI().getHost()+":"+task.getCrawlURI().getPort()+url;
			}else{
				url = Constants.HTTP_FILL_KEY+task.getCrawlURI().getHost()+url;
			}
			return new URL(url);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String executeJs(final String script){
		 ScriptEngineManager manager = new ScriptEngineManager();
		 ScriptEngine se = manager.getEngineByName("javascript");   
		 Object r = null;
		    try  {   
		      r = se.eval(script); 
		      if(null != r){
		    	  return (String)r;
		      }
		    } catch(Exception e) { 
//		    	 try  {
//			    	SwingUtilities.invokeLater(new Runnable() {
//						public void run() {
//							BrowserHelper.newInstance().evaluate(script); 
//						}
//					});
//		    	 } catch(Exception ex) { 
//		    		 ex.printStackTrace();
//		    	 }
//		    	if(null != r){
//			    	return (String)r;
//			    }
		    } 
		    return script;
	}

	/**
	 * 取得所有指定属性/标签的HTML,并放在LIST中
	 * @param doc HTML文档对象
	 * @param filter 过滤器
	 * @param list 
	 * @throws ParserException
	 */
	private void addHtmlToListByFilter(Document doc, Map<String,String[]> filter,
			List<String> list){
		log.info("========开始取得所有指定属性/标签的HTML,并放在LIST中=========");
		String[] values = null;
		Elements tags = null;
		for (String key : filter.keySet()) {
			//处理单标签
			if(Constants.SINGLE_TAG.equals(key)){
				values = filter.get(key);
				for(String v : values){
					//tags = doc.getElementsByTag(v);
					tags = doc.select(v);
					for(Element e : tags){
						if(StringUtils.isBlank(e.outerHtml())){
							continue;
						}
						list.add(e.outerHtml());
					}
				}
			}else{//否则查找属性
				values = filter.get(key);
				for(String v : values){
					tags = doc.getElementsByAttributeValueMatching(key, v);
					for(Element e : tags){
						if(StringUtils.isBlank(e.outerHtml())){
							continue;
						}
						list.add(e.outerHtml());
					}
				}
			}
		}
	}
	/**
	 * 取得所有指定属性/标签的HTML
	 * @param doc html文档对象
	 * @param filter 过滤器
	 */
	private void addPlainTextToList(Document doc, Map<String,String[]> filter,List<String> list,Map<String,String> contentHandleMap){
		String[] values = null;
		Elements tags = null;
		for (String key : filter.keySet()) {
			//处理单标签
			if(Constants.SINGLE_TAG.equals(key)){
//				values = filter.get(key);
//				for(String v : values){
//					//tags = doc.getElementsByTag(v);
//					tags = doc.select(v);
//					for(Element e : tags){
//						if(StringUtils.isBlank(e.text())){
//							continue;
//						}
//						list.add(contentHandle(e.text().replaceAll("\\s", ""),contentHandleMap));
//					}
//				}
				list.add(contentHandle(doc.text().replaceAll("\\s", ""),contentHandleMap));
			}else{//否则查找属性
				values = filter.get(key);
				for(String v : values){
					tags = doc.getElementsByAttributeValueMatching(key, v);
					for(Element e : tags){
						if(StringUtils.isBlank(e.text())){
							continue;
						}
						list.add(contentHandle(e.text().replaceAll("\\s", ""),contentHandleMap));
					}
				}
			}
		}
	}

	/**
	 * 解析并组装采集参数，支持标签属性/值形式和标签名称形式，可混合使用
	 * <li>约定采集参数格式如下</li>
	 * <li>1，标签属性/值形式，如：class=articleList.*|tips,id=fxwb|fxMSN|fxMSN
	 *      注意：属性值 支持正则表达式</li>
	 * <li>2，标签名称形式，如：div,p,span</li>
	 * <li>3，混合形式，如：class=articleList|tips,id=fxwb|fxMSN|fxMSN,div,p,span</li>
	 * @param paramMap 参数map
	 * @param str 参数字符串
	 */
//	private void populateParamMap(Map<String, String> paramMap,String paramStr) {
//		String[] paramStrArr = paramStr.split(",");
//		String[] tempStrArr = null;
//		StringBuilder sb = new StringBuilder();
//		for(String temp : paramStrArr){
//			if(temp.contains("=")){
//				tempStrArr = temp.split("=");
//				paramMap.put(tempStrArr[0], tempStrArr[1]);
//			}else{
//				if(StringUtils.isNotBlank(temp)){
//					sb.append(temp).append("|");
//				}
//			}
//		}
//		if(StringUtils.isNotBlank(sb.toString())){
//			paramMap.put(Constants.SINGLE_TAG, sb.substring(0, sb.length() - 1));
//		}
//	}
		
    /**
     * 测试方法-打开文件并返回内容
     * @param szFileName 文件绝对地址
     * @param charset 字符集
     * @return 内容
     */
	public static String openFile(String szFileName,String charset) {
		try {
			BufferedReader bis = new BufferedReader(new InputStreamReader(
					new FileInputStream(new File(szFileName)), charset));
			StringBuilder szContent = new StringBuilder();
			String szTemp;

			while ((szTemp = bis.readLine()) != null) {
				szContent.append(szTemp).append("\n");
			}
			bis.close();
			return szContent.toString();
		} catch (Exception e) {
			return "";
		}
	}

	public static void main(String[] args){
		Pattern p = Pattern.compile(Constants.RULE_POS_REGEX);
		Matcher m=p.matcher("111122{1}");
		while(m.find()){
		   System.out.println(Integer.valueOf(m.group()));
		}
	}
}
