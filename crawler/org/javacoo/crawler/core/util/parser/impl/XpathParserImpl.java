/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.crawler.core.util.parser.impl;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.javacoo.crawler.core.util.parser.Parser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Xpath解析实现类型
 * <p>
 * 说明:
 * </p>
 * <li>基于htmlcleaner-2.10实现</li>
 * 
 * @author DuanYong
 * @since 2015-2-15 下午4:00:12
 * @version 1.0
 */
public class XpathParserImpl implements Parser {
	private static Log log = LogFactory.getLog(XpathParserImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javacoo.crawler.core.util.parser.Parser#get(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String get(String html, String expression) {
		try {
			HtmlCleaner hc = new HtmlCleaner();
			TagNode tn = hc.clean(html);
			Document dom = new DomSerializer(new CleanerProperties()).createDOM(tn);
			XPath xPath = XPathFactory.newInstance().newXPath();
			Object result;
			result = xPath.evaluate(expression, dom, XPathConstants.NODESET);
			if (result instanceof NodeList) {
				NodeList nodeList = (NodeList) result;
				if (nodeList.getLength() == 0) {
					return "";
				}
				Node item = nodeList.item(0);
				if (item.getNodeType() == Node.ATTRIBUTE_NODE
						|| item.getNodeType() == Node.TEXT_NODE) {
					return item.getTextContent();
				} else {
					StreamResult xmlOutput = new StreamResult(new StringWriter());
					Transformer transformer = TransformerFactory.newInstance().newTransformer();
					transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
					transformer.transform(new DOMSource(item), xmlOutput);
					return xmlOutput.getWriter().toString();
				}
			}
			return result.toString();
		} catch (Exception e) {
			log.error("解析内容失败! " + expression, e);
		}
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.javacoo.crawler.core.util.parser.Parser#getList(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<String> getList(String html, String expression) {
		List<String> resultList = new ArrayList<String>();
		try {
			HtmlCleaner hc = new HtmlCleaner();
			TagNode tn = hc.clean(html);
			Document dom = new DomSerializer(new CleanerProperties()).createDOM(tn);
			XPath xPath = XPathFactory.newInstance().newXPath();
			Object result;
			result = xPath.evaluate(expression, dom, XPathConstants.NODESET);
			if (result instanceof NodeList) {
				NodeList nodeList = (NodeList) result;
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
				StreamResult xmlOutput = new StreamResult();
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,"yes");
				for (int i = 0; i < nodeList.getLength(); i++) {
					Node item = nodeList.item(i);
					if (item.getNodeType() == Node.ATTRIBUTE_NODE
							|| item.getNodeType() == Node.TEXT_NODE) {
						resultList.add(item.getTextContent());
						log.error("expression="+expression+",result="+item.getTextContent());
					} else {
						xmlOutput.setWriter(new StringWriter());
						transformer.transform(new DOMSource(item), xmlOutput);
						resultList.add(xmlOutput.getWriter().toString());
						log.error("expression="+expression+",result="+xmlOutput.getWriter().toString());
					}
				}
			} else {
				resultList.add(result.toString());
				log.error("expression="+expression+",result="+result.toString());
			}
		} catch (Exception e) {
			log.error("解析内容失败! " + expression, e);
		}
		return resultList;
	}

}
