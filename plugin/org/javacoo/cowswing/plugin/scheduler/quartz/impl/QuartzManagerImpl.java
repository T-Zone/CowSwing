package org.javacoo.cowswing.plugin.scheduler.quartz.impl;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.javacoo.cowswing.base.service.ICrawlerService;
import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.plugin.scheduler.constant.SchedulerConstant;
import org.javacoo.cowswing.plugin.scheduler.core.SchedulerTaskSvc;
import org.javacoo.cowswing.plugin.scheduler.quartz.QuartzManager;
import org.javacoo.cowswing.plugin.scheduler.service.beans.ScheduleTaskBean;
import org.javacoo.cowswing.plugin.scheduler.service.beans.SchedulerCriteria;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.stereotype.Component;

/**
 * 定时任务管理实现类
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2017年11月30日上午8:33:20
 */
@Component("quartzManagerBean")
public class QuartzManagerImpl implements QuartzManager{
	private Logger log = LoggerFactory.getLogger(QuartzManagerImpl.class);
	@Autowired
	private Scheduler scheduler;
	/**定时任务服务对象MAP*/
	@Autowired
	private Map<String,SchedulerTaskSvc> schedulerTaskSvcMap;
	/**
	 * 定时任务配置服务类
	 */
	@Resource(name="crawlerSchedulerService")
	private ICrawlerService<ScheduleTaskBean,SchedulerCriteria> crawlerSchedulerService;
    /**
     * 启动初始化任务
     * <p>说明:</p>
     * <li></li>
     * @throws Exception
     * @throws ParseException
     * @since 2017年11月30日上午8:45:42
     */
	@PostConstruct
	private void init() throws Exception, ParseException {
		SchedulerCriteria criteria = new SchedulerCriteria();
		criteria.setStatus(Constant.TASK_STATUS_RUN);
		List<ScheduleTaskBean> result = crawlerSchedulerService.getList(criteria,SchedulerConstant.SQLMAP_ID_LIST_CRAWLER_SCHEDULING);
		// 如果计划任务不存在并且数据库里的任务状态为可用时,则创建计划任务
		if(CollectionUtils.isNotEmpty(result)){
			org.javacoo.cowswing.plugin.scheduler.domain.Scheduler scheduleTask = null;
			for(ScheduleTaskBean tempSchedulerBean : result){
				scheduleTask = new org.javacoo.cowswing.plugin.scheduler.domain.Scheduler();
				BeanUtils.copyProperties(scheduleTask, tempSchedulerBean);
				initSchedule(scheduleTask);
			}
		}
	}
    /**
     * 初始启动任务
     * <p>说明:</p>
     * <li></li>
     * @param scheduleTask
     * @since 2017年11月30日上午8:45:33
     */
	private void initSchedule(org.javacoo.cowswing.plugin.scheduler.domain.Scheduler scheduleTask) {
		try {
			this.createCronTriggerBean(scheduleTask);
		} catch (Exception e) {
			log.error("初始启动抽奖任务失败:"+e.getMessage());
			e.printStackTrace();
		}
	}
   /**
    * 获取Trigger
    * <p>说明:</p>
    * <li></li>
    * @param scheduleTask
    * @return
    * @throws SchedulerException
    * @since 2017年11月30日上午8:47:00
    */
	private Trigger getTrigger(org.javacoo.cowswing.plugin.scheduler.domain.Scheduler scheduleTask) throws SchedulerException {
		// 运行时可通过动态注入的scheduler得到trigger
		Trigger trigger = scheduler.getTrigger(new TriggerKey(scheduleTask.getSchedulerId().toString()));
		return trigger;
	}

	/**
	 * 创建/添加计划任务
	 * <p>说明:</p>
	 * <li></li>
	 * @param scheduleTask
	 * @throws Exception
	 * @since 2017年11月30日上午8:46:52
	 */
	public void createCronTriggerBean(org.javacoo.cowswing.plugin.scheduler.domain.Scheduler scheduleTask) throws Exception {
		// 新建一个基于Spring的管理Job类
		MethodInvokingJobDetailFactoryBean mjdfb = new MethodInvokingJobDetailFactoryBean();
		mjdfb.setName(scheduleTask.getSchedulerId().toString());// 设置Job名称
		mjdfb.setTargetObject(schedulerTaskSvcMap.get(scheduleTask.getServiceName()));// 设置任务类
		mjdfb.setTargetMethod("start");// 设置任务方法
		mjdfb.setArguments(new Object[]{scheduleTask});
		mjdfb.setConcurrent(false); // 设置是否并发启动任务
		mjdfb.afterPropertiesSet();// 将管理Job类提交到计划管理类
		// 将Spring的管理Job类转为Quartz管理Job类
		JobDetail jobDetail = (JobDetail) mjdfb.getObject();
		//scheduler.addJob(jobDetail, true); // 将Job添加到管理类
		// 新一个基于Spring的时间类
		CronTriggerFactoryBean c = new CronTriggerFactoryBean();
		c.setCronExpression(scheduleTask.getExpression());// 设置时间表达式
		c.setName(scheduleTask.getSchedulerId().toString());// 设置名称
		c.setJobDetail(jobDetail);// 注入Job
		c.afterPropertiesSet();
		scheduler.scheduleJob(jobDetail, c.getObject());
		TriggerKey triggerKey = new TriggerKey(scheduleTask.getSchedulerId().toString());
		scheduler.rescheduleJob(triggerKey,c.getObject());// 刷新管理类
		log.info(new Date() + ": 启动" + scheduleTask.getName() + "计划任务");
	}

	/**
	 * 启动任务
	 * <p>说明:</p>
	 * <li></li>
	 * @author DuanYong
	 * @param id
	 * @since 2017年6月12日上午8:55:20
	 */
    public void start(org.javacoo.cowswing.plugin.scheduler.domain.Scheduler scheduleTask){
    	try {
			createCronTriggerBean(scheduleTask);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    /**
     * 停止任务
     */
	public void stop(org.javacoo.cowswing.plugin.scheduler.domain.Scheduler scheduleTask){
    	try {
    		Trigger trigger = getTrigger(scheduleTask);
			if(trigger != null){
				stopScheduler(scheduleTask, trigger);
			}
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
    /**
     * 停止任务
     * <p>说明:</p>
     * <li></li>
     * @param scheduleTask
     * @param trigger
     * @throws SchedulerException
     * @since 2017年11月30日上午8:48:54
     */
	private void stopScheduler(org.javacoo.cowswing.plugin.scheduler.domain.Scheduler scheduleTask, Trigger trigger) throws SchedulerException {
		// 不可用
		scheduler.pauseTrigger(trigger.getKey());// 停止触发器
		scheduler.unscheduleJob(trigger.getKey());// 移除触发器
		scheduler.deleteJob(trigger.getJobKey());// 删除任务
		if(log.isInfoEnabled()){
			log.info(new Date() + ": 停止" + scheduleTask.getName() + "计划任务");
		}
	}

	public Scheduler getScheduler() {
		return scheduler;
	}
	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}
	

}
