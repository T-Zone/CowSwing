package org.javacoo.cowswing.plugin.im;

import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

public class Test2 {
	public static void main(String[] args){
		File pathToBinary = new File("D:\\soft\\system\\geckodriver-v0.19.1-win64\\geckodriver.exe");
		//FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
		System.setProperty("webdriver.gecko.driver", pathToBinary.getAbsolutePath());  
		FirefoxOptions options = new FirefoxOptions();
		//options.setBinary(ffBinary);
		FirefoxProfile firefoxProfile = new FirefoxProfile();
		firefoxProfile.setPreference("permissions.default.stylesheet", 2);//去掉css 
		//firefoxProfile.setPreference("permissions.default.image", 2);//去掉图片
		firefoxProfile.setPreference("dom.ipc.plugins.enabled.libflashplayer.so",false);//去掉flash  
		options.setProfile(firefoxProfile);
		
		WebDriver driver = new FirefoxDriver(options);
		Point targetPosition = new Point(-10000,-100000);
		driver.manage().window().setPosition(targetPosition);
		driver.get("http://www.yidianzixun.com/channel/hot");
		System.out.println(driver.getPageSource());
		WebElement we = driver.findElement(By.className("channel-news"));
		System.out.println(we.getText());
		List<WebElement> weList = driver.findElements(By.cssSelector(".channel-news > a"));
		for(WebElement twe : weList){
			System.out.println(twe.getAttribute("href"));
		}
		//driver.get("http://www.yidianzixun.com/channel/c11");
		//System.out.println(driver.getPageSource());
		driver.close();
	}
}
