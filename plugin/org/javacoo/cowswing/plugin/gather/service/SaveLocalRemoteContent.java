/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.gather.service;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.javacoo.cowswing.base.service.ICrawlerService;
import org.javacoo.cowswing.core.cache.ICowSwingCacheManager;
import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.core.event.CowSwingEvent;
import org.javacoo.cowswing.core.event.CowSwingEventType;
import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentCommentBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentCommentCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentPaginationBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentPaginationCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentResourceBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentResourceCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerExtendFieldCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerRuleBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerRuleCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerTaskBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerTaskCriteria;
import org.javacoo.crawler.core.data.ContentBean;
import org.javacoo.crawler.core.data.CrawlerExtendFieldBean;
import org.javacoo.crawler.core.data.uri.CrawlResURI;
import org.javacoo.persistence.util.DBConnectionManager;

/**
 * 保存本地内容至远程数据库
 * 
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2014-9-24 下午3:14:01
 * @version 1.0
 */
public class SaveLocalRemoteContent extends AbstractSaveRemoteContent{
	
	/**
	 * @param crawlerRuleService
	 * @param crawlerContentService
	 * @param crawlerContentCommentService
	 * @param crawlerContentPaginationService
	 * @param crawlerExtendFieldService
	 * @param connectionManager
	 * @param ruleId
	 */
	public SaveLocalRemoteContent(
			ICrawlerService<CrawlerRuleBean, CrawlerRuleCriteria> crawlerRuleService,
			ICrawlerService<CrawlerContentBean, CrawlerContentCriteria> crawlerContentService,
			ICrawlerService<CrawlerContentCommentBean, CrawlerContentCommentCriteria> crawlerContentCommentService,
			ICrawlerService<CrawlerContentPaginationBean, CrawlerContentPaginationCriteria> crawlerContentPaginationService,
			ICrawlerService<CrawlerExtendFieldBean, CrawlerExtendFieldCriteria> crawlerExtendFieldService,
			ICrawlerService<CrawlerTaskBean,CrawlerTaskCriteria> crawlerTaskService,
			ICrawlerService<CrawlerContentResourceBean,CrawlerContentResourceCriteria> crawlerContentResourceService,
			DBConnectionManager connectionManager, 
			ICowSwingCacheManager crawlerCacheManager, 
			Integer ruleId) {
		super(crawlerRuleService,
				crawlerContentService,
				crawlerContentCommentService,
				crawlerContentPaginationService,
				crawlerExtendFieldService,
				crawlerTaskService,
				crawlerContentResourceService,
				connectionManager, 
				crawlerCacheManager, 
				ruleId
				);
	}
	
	/**
	 * 将指定规则的内容保存到远程数据库
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2013-4-30 下午9:06:13
	 * @version 1.0
	 * @exception 
	 * @param ruleId
	 */
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.plugin.gather.service.AbstractThreadService#doRun()
	 */
	@Override
	protected void doRun(){
		CrawlerRuleBean rule = new CrawlerRuleBean();
		rule.setRuleId(this.ruleId);
		rule = this.crawlerRuleService.get(rule, GatherConstant.SQLMAP_ID_GET_CRAWLER_RULE);
		if(Boolean.valueOf(rule.getRuleDataBaseBean().getSaveToDataBaseFlag())){
			CrawlerContentCriteria crawlerContentCriteria = new CrawlerContentCriteria();
			crawlerContentCriteria.setRuleId(ruleId);
			crawlerContentCriteria.setHasSave(Constant.NO);
			List<CrawlerContentBean> resultList = this.crawlerContentService.getList(crawlerContentCriteria, GatherConstant.SQLMAP_ID_GET_LIST_CRAWLER_CONTENT);
			if(CollectionUtils.isNotEmpty(resultList)){
				ContentBean contentBean = null;
				List<CrawlerContentCommentBean> commentList = null;
				CrawlerContentCommentCriteria crawlerContentCommentCriteria = new CrawlerContentCommentCriteria();
				
				List<CrawlerContentPaginationBean> contentPageList = null;
				CrawlerContentPaginationCriteria crawlerContentPaginationCriteria = new CrawlerContentPaginationCriteria();
				
				List<CrawlerContentResourceBean> contentResList = null;
				CrawlerContentResourceCriteria crawlerContentResourceCriteria = new CrawlerContentResourceCriteria();
				
				
				List<CrawlerExtendFieldBean> extendFieldList = null;
				CrawlerExtendFieldCriteria crawlerExtendFieldCriteria = new CrawlerExtendFieldCriteria();
				for(CrawlerContentBean crawlerContentBean : resultList){
					contentBean = new ContentBean();
					contentBean.setBrief(crawlerContentBean.getBrief());
					contentBean.setContent(crawlerContentBean.getContent());
					contentBean.setTitle(crawlerContentBean.getTitle());
					contentBean.setTitleImg(crawlerContentBean.getTitleImg());
					
					crawlerContentCommentCriteria.setContentId(crawlerContentBean.getContentId());
					commentList = this.crawlerContentCommentService.getList(crawlerContentCommentCriteria, GatherConstant.SQLMAP_ID_GET_LIST_CRAWLER_CONTENT_COMMENT);
					if(CollectionUtils.isNotEmpty(commentList)){
						for(CrawlerContentCommentBean crawlerContentCommentBean : commentList){
							contentBean.getCommentList().add(crawlerContentCommentBean.getContent());
						}
					}
					crawlerContentPaginationCriteria.setContentId(crawlerContentBean.getContentId());
					contentPageList = this.crawlerContentPaginationService.getList(crawlerContentPaginationCriteria, GatherConstant.SQLMAP_ID_GET_LIST_CRAWLER_CONTENT_PAGINATION);
					if(CollectionUtils.isNotEmpty(contentPageList)){
						for(CrawlerContentPaginationBean crawlerContentPaginationBean : contentPageList){
							contentBean.getContentList().add(crawlerContentPaginationBean.getContent());
						}
					}
					crawlerContentResourceCriteria.setContentId(crawlerContentBean.getContentId());
					contentResList = this.crawlerContentResourceService.getList(crawlerContentResourceCriteria,GatherConstant.SQLMAP_ID_GET_LIST_CRAWLER_CONTENT_RESOURCE);
					if(CollectionUtils.isNotEmpty(contentResList)){
						CrawlResURI crawlResURI = null;
						for(CrawlerContentResourceBean crawlerContentResourceBean : contentResList){
							crawlResURI = new CrawlResURI();
							crawlResURI.setNewResUrl(crawlerContentResourceBean.getPath());
							crawlResURI.setName(crawlerContentResourceBean.getName());
							crawlResURI.setDesc(crawlerContentResourceBean.getName());
							contentBean.getResCrawlURIList().add(crawlResURI);
						}
					}
					crawlerExtendFieldCriteria.setContentId(crawlerContentBean.getContentId());
					extendFieldList = this.crawlerExtendFieldService.getList(crawlerExtendFieldCriteria, GatherConstant.SQLMAP_ID_GET_LIST_CRAWLER_EXTEND_FIELD);
					if(CollectionUtils.isNotEmpty(extendFieldList)){
						for(CrawlerExtendFieldBean crawlerExtendFieldBean : extendFieldList){
							contentBean.getFieldValueMap().put(crawlerExtendFieldBean.getFieldName(), crawlerExtendFieldBean.getFieldValue());
						}
					}
					//插入内容
					insertContentToTargetDataBase(contentBean,rule);
					randomValueMap.clear();
					randomValue.clear();
					//修改状态
					updateContentState(crawlerContentBean);
					updateRuleState(rule);
				}
			}
		}
	}
	
	
	/**
	 * 更新规则状态
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2014-9-24 下午4:20:24
	 * @version 1.0
	 * @exception 
	 * @param rule
	 */
	private void updateRuleState(CrawlerRuleBean rule) {
		CrawlerRuleBean ruleBean = new CrawlerRuleBean();
		ruleBean.setRuleId(rule.getRuleId());
		ruleBean.setTotalItem(rule.getTotalItem() + 1);
		ruleBean.setRuleType(Constant.YES);
		ruleBean.setCowSwingEvent(new CowSwingEvent(this,CowSwingEventType.LocalRuleTableUpdateEvent));
		crawlerRuleService.update(ruleBean, GatherConstant.SQLMAP_ID_UPDATE_CRAWLER_RULE);
	}

	
	
	
	

	
	/**
	 * 根据字段类型设置值
	 * <p>方法说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2013-3-4 上午8:54:29
	 * @param pstm
	 * @param type
	 * @param pos
	 * @param value
	 * @param valueType
	 * @return void
	 */
	protected void setValueByType(ContentBean contentBean,CrawlerRuleBean rule,PreparedStatement pstm,String type,int pos,String value,String valueType,String ky,String staticValue){
		log.info("字段类型:"+type);
		log.info("字段值:"+value);
		log.info("值类型:"+valueType);
		log.info("静态值:"+staticValue);
		try {
			//如果是动态值
			if(GatherConstant.EXTEND_FIELDS_VALUE_TYPE_DYNAMIC.equals(valueType) || GatherConstant.EXTEND_FIELDS_VALUE_TYPE_DYNAMIC_MAP_KEY.equals(valueType) || GatherConstant.EXTEND_FIELDS_VALUE_TYPE_DYNAMIC_MAP_VALUE.equals(valueType)){
				if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT.equals(value)){
					value = contentBean.getContent();
				}else if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_ID.equals(value)){
					value = ky;
				}else if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_TITLE.equals(value)){
					value = contentBean.getTitle();
				}else if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_BRIEF.equals(value)){
					value = contentBean.getBrief();
				}else if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_VIEW_DATE.equals(value)){
					value = String.valueOf(getRandomDate(rule).getTime());
				}else if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_TITLE_IMG.equals(value)){
					value = contentBean.getTitleImg();
				}else if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_RESOURCE.equals(value)){
					value = staticValue;
				}else if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_RESOURCE_NAME.equals(value)){
					value = staticValue;
				}else if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_RESOURCE_DESC.equals(value)){
					value = staticValue;
				}else if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_PAGE.equals(value)){
					value = staticValue;
				}else if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_COMMENT.equals(value)){
					value = staticValue;
				}else if(GatherConstant.EXTEND_FIELDS_VALUE_TYPE_DYNAMIC_MAP_KEY.equals(valueType)){
					
				}else if(GatherConstant.EXTEND_FIELDS_VALUE_TYPE_DYNAMIC_MAP_VALUE.equals(valueType)){
					if(null != contentBean.getFieldValueMap() && StringUtils.isNotBlank(contentBean.getFieldValueMap().get(value))){
						value = contentBean.getFieldValueMap().get(value);
					}
				}else{
					if(null != contentBean.getFieldValueMap() && StringUtils.isNotBlank(contentBean.getFieldValueMap().get(value))){
						value = contentBean.getFieldValueMap().get(value);
					}
				}
			}else{//静态值
				if(GatherConstant.EXTEND_FIELDS_VALUE_TYPE_STATIC.equals(valueType) || GatherConstant.EXTEND_FIELDS_VALUE_TYPE_STATIC_MULIT.equals(valueType)){
					value = getRandomValue(value);
				}else{
					value = getMapRandomValue(value,valueType);
				}
			}
			log.info("动态值:"+value);
			Map<String,String> typeMap = rule.getRuleDataBaseBean().getDataTypeMap(this.crawlerCacheManager);
			if(null == typeMap || typeMap.isEmpty()){
				log.error("未配置数据类型映射："+rule.getRuleDataBaseBean().getDataBaseType());
				return;
			}
			if(GatherConstant.BigDecimal.equals(typeMap.get(type))){
				pstm.setBigDecimal(pos, new BigDecimal(value));
			}else if(GatherConstant.Blob.equals(typeMap.get(type))){
				pstm.setBlob(pos, new ByteArrayInputStream(value.getBytes()));
			}else if(GatherConstant.Boolean.equals(typeMap.get(type))){
				pstm.setBoolean(pos, new Boolean(value));
			}else if(GatherConstant.Byte.equals(typeMap.get(type))){
				pstm.setByte(pos, new Byte(value));
			}else if(GatherConstant.Clob.equals(typeMap.get(type))){
				pstm.setClob(pos,new javax.sql.rowset.serial.SerialClob(value.toCharArray()));
			}else if(GatherConstant.Date.equals(typeMap.get(type))){
				Date date = new Date(Long.valueOf(value));
				pstm.setDate(pos,new java.sql.Date(date.getTime()));
			}else if(GatherConstant.Double.equals(typeMap.get(type))){
				pstm.setDouble(pos, new Double(value));
			}else if(GatherConstant.Float.equals(typeMap.get(type))){
				pstm.setFloat(pos, new Float(value));
			}else if(GatherConstant.Int.equals(typeMap.get(type))){
				pstm.setInt(pos,Integer.valueOf(value));
			}else if(GatherConstant.Long.equals(typeMap.get(type))){
				pstm.setLong(pos, new Long(value));
			}else if(GatherConstant.Short.equals(typeMap.get(type))){
				pstm.setShort(pos, new Short(value));
			}else if(GatherConstant.Time.equals(typeMap.get(type))){
				Date date = new Date(Long.valueOf(value));
				pstm.setTime(pos,new java.sql.Time(date.getTime()));
			}else if(GatherConstant.Timestamp.equals(typeMap.get(type))){
				Date date = new Date(Long.valueOf(value));
				pstm.setTimestamp(pos,new java.sql.Timestamp(date.getTime()));
			}else{
				pstm.setString(pos,value);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}

}
