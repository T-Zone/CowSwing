package org.javacoo.cowswing.plugin.gather.service.export.manager;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.util.StringUtil;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;



/**
 * EXCEL下载文件模板管理
 * @author DuanYong
 * 2012-07-21
 */
public class ExcelTemplateManager implements TemplateManager<Resource>{ 
	private static Log logger = LogFactory.getLog(ExcelTemplateManager.class);
	
	private static Map<String, Resource> templateConfigs = null;
	
	private final static String DEFAULT_EXCEL_TEMPLATEFILE = "templates/*.xls";
	private String strTemplateFile;

	/**
	 * 根据配置文件初始化 templateConfigs
	 */
    public ExcelTemplateManager(String strTemplateFile){
    	this.strTemplateFile = strTemplateFile;
    	templateConfigs = new HashMap<String, Resource>();
    	genRealTemplates();
    	logger.info("初始化加载excel模板完成");
    }
	/**
	 * 根据模板ID取得模板
	 * <p>方法说明:</p>
	 * @since 2012-7-26 下午4:15:12
	 * @param templateId 模板ID
	 * @return T
	 */
	public Resource findTemplate(String templateId){
		return templateConfigs.get(templateId);
	}
	
	private void genRealTemplates() {
		String[] aryConfigFile = null;
		if(!StringUtils.isNotBlank(strTemplateFile)){
			aryConfigFile = new String[]{DEFAULT_EXCEL_TEMPLATEFILE};
		}else{
			aryConfigFile = StringUtils.split(strTemplateFile, ",");
		}
		String templateDir = null;
		String strFileUrl = null;
		String strFileName = null;
		String strFileId = null;
		for(String strResource:aryConfigFile)
		{
			ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
			try {
				Resource[] resources = resourcePatternResolver.getResources(strResource);
				templateDir = strTemplateFile.substring(0,strTemplateFile.lastIndexOf('/'));
				
				for(int i =0;i<resources.length;i++){
					strFileUrl = resources[i].getURL().toString();
					strFileId = strFileUrl.substring(strFileUrl.lastIndexOf('/')+1,strFileUrl.lastIndexOf('.'));
					strFileName = strFileUrl.substring(strFileUrl.lastIndexOf('/')+1);
					templateConfigs.put(strFileId,resources[i]);
					logger.info(String.format("加载模板配置%s成功",strFileName));
				}
			} catch (IOException e) {
				e.printStackTrace();
				logger.error("加载模板配置文本发生IO错误");
			}
			catch(Exception e){
				logger.error(String.format("根据配置%s获取配置文件名出错",templateDir+strFileName),e);
				e.printStackTrace();
				logger.error("解析配置文件出错");
			}
		}
	}
	
	
	public String getStrTemplateDir() {
		return strTemplateFile;
	}
	public void setStrTemplateDir(String strTemplateDir) {
		this.strTemplateFile = strTemplateDir;
	}

	public String getStrTemplateFile() {
		return strTemplateFile;
	}

	public void setStrTemplateFile(String strTemplateFile) {
		this.strTemplateFile = strTemplateFile;
	}
	
	
}