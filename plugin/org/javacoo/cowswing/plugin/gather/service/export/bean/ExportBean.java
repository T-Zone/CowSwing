package org.javacoo.cowswing.plugin.gather.service.export.bean;
/**
 * 数据导出对象
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18下午3:48:29
 * @version 1.0
 */
public class ExportBean {
	/**内容类型*/
	private String contentType;
	/**文件名称*/
	private String fileName;
	/**内容*/
	private String content;
	/**byte内容*/
	private byte[] byteContent;
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public byte[] getByteContent() {
		return byteContent;
	}
	public void setByteContent(byte[] byteContent) {
		this.byteContent = byteContent;
	}
	
    
}
