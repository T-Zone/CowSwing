package org.javacoo.cowswing.plugin.gather.service.export.manager;

/**
 * 模板管理接口
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18上午9:33:23
 * @version 1.0
 */
public interface TemplateManager<T> { 
	/**
	 * 根据模板ID取得模板
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-2-18上午9:33:34
	 * @version 1.0
	 * @param templateId 模板ID
	 * @return 模板
	 */
	T findTemplate(String templateId);
}