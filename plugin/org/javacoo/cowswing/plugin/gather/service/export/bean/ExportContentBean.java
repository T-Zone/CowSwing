package org.javacoo.cowswing.plugin.gather.service.export.bean;
/**
 * 导出数据对象
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18上午10:59:10
 * @version 1.0
 */
public class ExportContentBean {
	/**下载类型*/
	private String downloadType;
	/**文件名*/
	private String fileName;
	/**组装好的下载文件byte[]对象*/
	private byte[] byteContent;
	/**组装好的下载文件字符串*/
	private String stringContent;
	public String getDownloadType() {
		return downloadType;
	}
	public void setDownloadType(String downloadType) {
		this.downloadType = downloadType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public byte[] getByteContent() {
		return byteContent;
	}
	public void setByteContent(byte[] byteContent) {
		this.byteContent = byteContent;
	}
	public String getStringContent() {
		return stringContent;
	}
	public void setStringContent(String stringContent) {
		this.stringContent = stringContent;
	}
	
	

}
