package org.javacoo.cowswing.plugin.gather.service.beans;

import java.util.Map;

import org.javacoo.cowswing.core.cache.ICowSwingCacheManager;
import org.javacoo.cowswing.core.cache.support.CacheKeyConstant;

/**
 * 采集规则远程数据库配置
 *@author DuanYong
 *@since 2013-3-2下午7:46:00
 *@version 1.0
 */
public class RuleDataBaseBean {
	/**是否保存至指定数据库*/
	private java.lang.String saveToDataBaseFlag;
	/**是否直接保存*/
	private java.lang.String onlySaveRemoteataBaseFlag;
	/**数据库ID*/
	private java.lang.String dataBaseId;
	/**数据库类型*/
	private java.lang.String dataBaseType;
	/**主表名称*/
	private java.lang.String primaryTable;
	/**内容分页标签*/
	private java.lang.String contentPageTag;
	/**主键生成策略*/
	private java.lang.String primaryGen;
	/**主键表*/
	private java.lang.String primaryGenTable;
	/**主键表字段名称*/
	private java.lang.String primaryGenTableColumnName;
	/**主键表名称*/
	private java.lang.String primaryGenTableColumnValue;
	/**主键表PK值名称*/
	private java.lang.String primaryGenPkValueColumnName;
	/**数据值信息map*/
    private Map<String,Map<String,ColumnBean>> hasSelectedValueMap;
    
    private Map<String,String> dataTypeMap;

	public java.lang.String getSaveToDataBaseFlag() {
		return saveToDataBaseFlag;
	}
	public void setSaveToDataBaseFlag(java.lang.String saveToDataBaseFlag) {
		this.saveToDataBaseFlag = saveToDataBaseFlag;
	}
	
	/**
	 * @return the onlySaveRemoteataBaseFlag
	 */
	public java.lang.String getOnlySaveRemoteataBaseFlag() {
		return onlySaveRemoteataBaseFlag;
	}
	/**
	 * @param onlySaveRemoteataBaseFlag the onlySaveRemoteataBaseFlag to set
	 */
	public void setOnlySaveRemoteataBaseFlag(
			java.lang.String onlySaveRemoteataBaseFlag) {
		this.onlySaveRemoteataBaseFlag = onlySaveRemoteataBaseFlag;
	}
	public java.lang.String getDataBaseId() {
		return dataBaseId;
	}
	public void setDataBaseId(java.lang.String dataBaseId) {
		this.dataBaseId = dataBaseId;
	}
	
	public java.lang.String getDataBaseType() {
		return dataBaseType;
	}
	public void setDataBaseType(java.lang.String dataBaseType) {
		this.dataBaseType = dataBaseType;
	}
	/**
	 * @return the primaryTable
	 */
	public java.lang.String getPrimaryTable() {
		return primaryTable;
	}
	/**
	 * @param primaryTable the primaryTable to set
	 */
	public void setPrimaryTable(java.lang.String primaryTable) {
		this.primaryTable = primaryTable;
	}
	
	
	public java.lang.String getPrimaryGen() {
		return primaryGen;
	}
	public void setPrimaryGen(java.lang.String primaryGen) {
		this.primaryGen = primaryGen;
	}
	public Map<String, Map<String, ColumnBean>> getHasSelectedValueMap() {
		return hasSelectedValueMap;
	}
	public void setHasSelectedValueMap(
			Map<String, Map<String, ColumnBean>> hasSelectedValueMap) {
		this.hasSelectedValueMap = hasSelectedValueMap;
	}
	/**
	 * @return the contentPageTag
	 */
	public java.lang.String getContentPageTag() {
		return contentPageTag;
	}
	/**
	 * @param contentPageTag the contentPageTag to set
	 */
	public void setContentPageTag(java.lang.String contentPageTag) {
		this.contentPageTag = contentPageTag;
	}
	public java.lang.String getPrimaryGenTable() {
		return primaryGenTable;
	}
	public void setPrimaryGenTable(java.lang.String primaryGenTable) {
		this.primaryGenTable = primaryGenTable;
	}
	public java.lang.String getPrimaryGenTableColumnName() {
		return primaryGenTableColumnName;
	}
	public void setPrimaryGenTableColumnName(
			java.lang.String primaryGenTableColumnName) {
		this.primaryGenTableColumnName = primaryGenTableColumnName;
	}
	public java.lang.String getPrimaryGenTableColumnValue() {
		return primaryGenTableColumnValue;
	}
	public void setPrimaryGenTableColumnValue(
			java.lang.String primaryGenTableColumnValue) {
		this.primaryGenTableColumnValue = primaryGenTableColumnValue;
	}
	public java.lang.String getPrimaryGenPkValueColumnName() {
		return primaryGenPkValueColumnName;
	}
	public void setPrimaryGenPkValueColumnName(
			java.lang.String primaryGenPkValueColumnName) {
		this.primaryGenPkValueColumnName = primaryGenPkValueColumnName;
	}
	
	public Map<String,String> getDataTypeMap(ICowSwingCacheManager crawlerCacheManager){
		if(null == dataTypeMap){
			Map<String,Map<String,String>> typeMap = (Map<String,Map<String,String>>) crawlerCacheManager.getValue(CacheKeyConstant.CACHE_KEY_DATA_TYPE_MAP);
			dataTypeMap = typeMap.get(this.getDataBaseType());
		}
		return dataTypeMap;
	}
	
	
}
