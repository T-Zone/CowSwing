/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.gather.ui.view.panel;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.lang.StringUtils;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerDataBaseBean;
import org.javacoo.cowswing.ui.view.panel.AbstractContentPanel;
import org.javacoo.cowswing.ui.view.panel.GBC;
import org.springframework.stereotype.Component;

/**
 * 数据库参数配置
 *@author DuanYong
 *@since 2013-2-14下午6:08:12
 *@version 1.0
 */
@Component("dataBaseSettingPanel")
public class DataBaseSettingPanel extends AbstractContentPanel<CrawlerDataBaseBean>{
	private static final long serialVersionUID = 1L;

	/**容器面板*/
	protected JComponent centerPane;
	/**数据库信息描述输入框*/
	private javax.swing.JTextField descField;
	/**数据库信息描述标签*/
	private javax.swing.JLabel descLabel;
	/**驱动名称输入框*/
	private javax.swing.JTextField classNameField;
	/**驱动名称标签*/
	private javax.swing.JLabel classNameLabel;
	/**url输入框*/
	private javax.swing.JTextField urlField;
	/**url标签*/
	private javax.swing.JLabel urlLabel;
	/**用户名输入框*/
	private javax.swing.JTextField userNameField;
	/**用户名标签*/
	private javax.swing.JLabel userNameLabel;
	/**密码输入框*/
	private javax.swing.JTextField passwordField;
	/**密码标签*/
	private javax.swing.JLabel passwordLabel;
	/**数据库类型*/
	private JComboBox typeCombo;
	/**密码标签*/
	private javax.swing.JLabel typeLabel;
	/**
	 * 初始化面板控件
	 */
	@Override
	protected void initComponents() {
		this.setLayout(new BorderLayout());
		if(null == centerPane){
			centerPane = new JPanel();
			centerPane.setLayout(new GridBagLayout());
			
			descLabel = new javax.swing.JLabel();
			descField = new javax.swing.JTextField();
			
			String[] data = new String[CrawlerDataBaseBean.DATABASE_TYPE_MAP.size()];
			int i = 0;
			for(Iterator<String> it = CrawlerDataBaseBean.DATABASE_TYPE_MAP.keySet().iterator();it.hasNext();){
				data[i] = it.next();
				i++;
			}
			
			
			typeLabel = new javax.swing.JLabel();
			typeCombo = new JComboBox(data);
			typeCombo.setSize(50, 20);
			
			classNameLabel = new javax.swing.JLabel();
			classNameField = new javax.swing.JTextField();
			
			urlLabel = new javax.swing.JLabel();
			urlField = new javax.swing.JTextField();
			
			userNameLabel = new javax.swing.JLabel();
			userNameField = new javax.swing.JTextField();
			
			passwordLabel = new javax.swing.JLabel();
			passwordField = new javax.swing.JTextField();
			
			
			
			
			
			
			
			
			descLabel.setText(LanguageLoader.getString("System.DataBase_desc"));
			centerPane.add(descLabel,new GBC(0,0).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));

			descField.setColumns(40);
			centerPane.add(descField,new GBC(1,0).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			typeLabel.setText(LanguageLoader.getString("System.DataBase_type"));
			centerPane.add(typeLabel,new GBC(0,1).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			centerPane.add(typeCombo,new GBC(1,1).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			classNameLabel.setText(LanguageLoader.getString("System.DataBase_className"));
			centerPane.add(classNameLabel,new GBC(0,2).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));

			classNameField.setColumns(20);
			centerPane.add(classNameField,new GBC(1,2).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			
			urlLabel.setText(LanguageLoader.getString("System.DataBase_url"));
			centerPane.add(urlLabel,new GBC(0,3).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));

			urlField.setColumns(20);
			centerPane.add(urlField,new GBC(1,3).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			
			userNameLabel.setText(LanguageLoader.getString("System.DataBase_userName"));
			centerPane.add(userNameLabel,new GBC(0,4).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));


			userNameField.setColumns(20);
			centerPane.add(userNameField,new GBC(1,4).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			
			passwordLabel.setText(LanguageLoader.getString("System.DataBase_password"));
			centerPane.add(passwordLabel,new GBC(0,5).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));

			passwordField.setColumns(20);
			centerPane.add(passwordField,new GBC(1,5).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			centerPane.add(new JLabel(),new GBC(0,6,2,1).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 1));
			add(centerPane,BorderLayout.NORTH);
		}
		
		
	}
	
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#populateData()
	 */
	@Override
	protected CrawlerDataBaseBean populateData() {
		CrawlerDataBaseBean crawlerDataBaseBean = new CrawlerDataBaseBean();
		crawlerDataBaseBean.setClassName(classNameField.getText());
		crawlerDataBaseBean.setDescription(descField.getText());
		crawlerDataBaseBean.setPassword(passwordField.getText());
		crawlerDataBaseBean.setType(typeCombo.getSelectedItem().toString());
		crawlerDataBaseBean.setUrl(urlField.getText());
		crawlerDataBaseBean.setUserName(userNameField.getText());
		return crawlerDataBaseBean;
	}

	

	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#fillComponentData(java.lang.Object)
	 */
	@Override
	protected void fillComponentData(CrawlerDataBaseBean t) {
		logger.info("填充页面控件数据");
		descField.setText(t.getDescription());
		if(StringUtils.isNotBlank(t.getType())){
			typeCombo.setSelectedItem(t.getType());
		}
		classNameField.setText(t.getClassName());
		urlField.setText(t.getUrl());
		userNameField.setText(t.getUserName());
	}
	/**
	 * 初始化监听事件
	 */
	protected void initActionListener(){
		typeCombo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				urlField.setText(CrawlerDataBaseBean.DATABASE_TYPE_URL_MAP.get(typeCombo.getSelectedItem().toString()));
				classNameField.setText(CrawlerDataBaseBean.DATABASE_TYPE_CLASSNAME_MAP.get(typeCombo.getSelectedItem().toString()));
			}
		});
	}
	public void initData(CrawlerDataBaseBean t){
		if(t == null){
			t = new CrawlerDataBaseBean();
		}
		fillComponentData(t);
	}
}
