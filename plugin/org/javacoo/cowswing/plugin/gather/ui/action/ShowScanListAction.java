package org.javacoo.cowswing.plugin.gather.ui.action;

import java.awt.event.ActionEvent;

import javax.annotation.Resource;
import javax.swing.AbstractAction;

import org.javacoo.cowswing.core.loader.ImageLoader;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.plugin.gather.ui.view.panel.LocalRuleListPage;
import org.javacoo.cowswing.plugin.gather.ui.view.panel.ScanRuleListPage;
import org.javacoo.cowswing.ui.view.panel.PageContainer;
import org.springframework.stereotype.Component;

/**
 * 展示全站扫描规则列表
 * 
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2015-3-2 下午9:00:01
 * @version 1.0
 */
@Component("showScanListAction")
public class ShowScanListAction extends AbstractAction{
	private static final long serialVersionUID = 1L;
	@Resource(name="scanRuleListPage")
    private ScanRuleListPage ruleListPage;
	@Resource(name="pageContainer")
    private PageContainer pageContainer;
	
	public ShowScanListAction(){
		super(LanguageLoader.getString("Scan.scanList"),ImageLoader.getImageIcon("CrawlerResource.scan"));
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		pageContainer.addPage(ruleListPage, ruleListPage.getPageId());
		ruleListPage.init();
	}

	/**
	 * @return the ruleListPage
	 */
	public ScanRuleListPage getRuleListPage() {
		return ruleListPage;
	}
	

}
